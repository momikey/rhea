#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/test/data/monomorphic.hpp>

#include <algorithm>
#include <string>
#include <memory>
#include <vector>

#include "../../include/inference/engine.hpp"
#include "../../include/ast.hpp"
#include "../../include/state/module_tree.hpp"
#include "../../include/util/compat.hpp"

namespace data = boost::unit_test::data;
namespace util = rhea::util;

namespace {
    using namespace rhea::inference;
    using namespace rhea::ast;
    using namespace rhea::types;
    using namespace rhea::state;

    struct InferenceFixture
    {
        InferenceFixture()
        {
            engine.module_scopes["main"] = std::make_unique<ModuleScopeTree>("main");
            engine.visitor.module_scope = engine.module_scopes["main"].get();
        }

        TypeEngine engine;
    };

    BOOST_FIXTURE_TEST_SUITE (inference_engine, InferenceFixture)

    BOOST_AUTO_TEST_CASE (engine_creation)
    {
        BOOST_TEST_MESSAGE("Testing type inference engine creation");
        BOOST_TEST(1 == 1);
    }

    BOOST_AUTO_TEST_CASE (infer_boolean_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of boolean literal");

        std::unique_ptr<ASTNode> node = make_expression<Boolean>(true);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Boolean));
    }

    BOOST_AUTO_TEST_CASE (infer_integer_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of integer literal");

        std::unique_ptr<ASTNode> node = make_expression<Integer>(42);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_byte_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of byte literal");

        std::unique_ptr<ASTNode> node = make_expression<Byte>(42);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Byte));
    }

    BOOST_AUTO_TEST_CASE (infer_long_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of long literal");

        std::unique_ptr<ASTNode> node = make_expression<Long>(42L);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Long));
    }

    BOOST_AUTO_TEST_CASE (infer_uinteger_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of unsigned integer literal");

        std::unique_ptr<ASTNode> node = make_expression<UnsignedInteger>(42);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::UnsignedInteger));
    }

    BOOST_AUTO_TEST_CASE (infer_ubyte_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of unsigned byte literal");

        std::unique_ptr<ASTNode> node = make_expression<UnsignedByte>(42);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::UnsignedByte));
    }

    BOOST_AUTO_TEST_CASE (infer_ulong_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of unsigned long literal");

        std::unique_ptr<ASTNode> node = make_expression<UnsignedLong>(42L);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::UnsignedLong));
    }

    BOOST_AUTO_TEST_CASE (infer_float_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of float literal");

        std::unique_ptr<ASTNode> node = make_expression<Float>(3.14f);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Float));
    }

    BOOST_AUTO_TEST_CASE (infer_double_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of double literal");

        std::unique_ptr<ASTNode> node = make_expression<Double>(3.14);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Double));
    }

    BOOST_AUTO_TEST_CASE (infer_symbol_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of symbol literal");

        std::unique_ptr<ASTNode> node = make_expression<Symbol>("foo");

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Symbol));
    }

    BOOST_AUTO_TEST_CASE (infer_nothing_literal)
    {
        BOOST_TEST_MESSAGE("Testing inference of nothing literal");

        std::unique_ptr<ASTNode> node = make_expression<Nothing>();

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        BOOST_TEST((util::get_if<NothingType>(&inferred.type()) != nullptr));
    }

    BOOST_AUTO_TEST_CASE (infer_binop)
    {
        BOOST_TEST_MESSAGE("Testing inference of binary operation");

        auto l = make_expression<Integer>(42);
        auto r = make_expression<Integer>(69);
        std::unique_ptr<ASTNode> node = make_expression<BinaryOp>(
            BinaryOperators::Add,
            std::move(l),
            std::move(r)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_ternary_op)
    {
        BOOST_TEST_MESSAGE("Testing inference of ternary operation");

        auto t = make_expression<Integer>(42);
        auto f = make_expression<Integer>(69);
        auto c = make_expression<Identifier>("foo");
        std::unique_ptr<ASTNode> node = make_expression<TernaryOp>(
            std::move(c),
            std::move(t),
            std::move(f)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_unary_op)
    {
        BOOST_TEST_MESSAGE("Testing inference of unary operation");

        auto o = make_expression<Integer>(42);
        std::unique_ptr<ASTNode> node = make_expression<UnaryOp>(
            UnaryOperators::Minus,
            std::move(o)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_coerce_op)
    {
        BOOST_TEST_MESSAGE("Testing inference of coercion operation");

        auto o = make_expression<Integer>(42);
        std::unique_ptr<ASTNode> node = make_expression<UnaryOp>(
            UnaryOperators::Coerce,
            std::move(o)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Promoted));
    }

    BOOST_AUTO_TEST_CASE (infer_reference_op)
    {
        BOOST_TEST_MESSAGE("Testing inference of reference operation");

        auto o = make_expression<Integer>(42);
        std::unique_ptr<ASTNode> node = make_expression<UnaryOp>(
            UnaryOperators::Ref,
            std::move(o)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_ref = util::get_if<ReferenceType>(&inferred.type());
        BOOST_TEST((as_ref != nullptr));
        auto as_simple = util::get_if<SimpleType>(&as_ref->referred_type->type());
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_pointer_op)
    {
        BOOST_TEST_MESSAGE("Testing inference of pointer operation");

        auto o = make_expression<Integer>(42);
        std::unique_ptr<ASTNode> node = make_expression<UnaryOp>(
            UnaryOperators::Ptr,
            std::move(o)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_ptr = util::get_if<PointerType>(&inferred.type());
        BOOST_TEST((as_ptr != nullptr));
        auto as_simple = util::get_if<SimpleType>(&as_ptr->pointed_type->type());
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }
    
    BOOST_AUTO_TEST_CASE (infer_dereference_op)
    {
        BOOST_TEST_MESSAGE("Testing inference of dereference operation");

        auto o = make_expression<Integer>(42);
        auto p = make_expression<UnaryOp>(UnaryOperators::Ptr, std::move(o));
        std::unique_ptr<ASTNode> node = make_expression<UnaryOp>(
            UnaryOperators::Dereference,
            std::move(p)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_for_loop)
    {
        BOOST_TEST_MESSAGE("Testing inference of for loop");

        auto o = make_expression<Integer>(42);
        std::vector<std::unique_ptr<Expression>> ov;
        ov.push_back(std::move(o));
        auto a = make_expression<Array>(ov);
        std::vector<std::unique_ptr<Statement>> empty_body;
        auto body = make_statement<Block>(empty_body);

        std::unique_ptr<ASTNode> node = make_statement<For>(
            "i",
            std::move(a),
            std::move(body)
        );

        node->visit(&engine.visitor);

        auto scope_tree = engine.module_scopes["main"].get();
        auto scope = scope_tree->root->children[0].get();
        BOOST_TEST((scope != nullptr));
        
        auto symbol = scope->symbol_table.find("i");
        BOOST_TEST((symbol != scope->symbol_table.end()));
        BOOST_TEST((symbol->second == node.get()));
    }

    BOOST_AUTO_TEST_CASE (infer_variable_definition)
    {
        BOOST_TEST_MESSAGE("Testing inference of variable definition");

        auto l = std::make_unique<Identifier>("foo");
        auto r = make_expression<Integer>(69);
        std::unique_ptr<ASTNode> node = make_statement<Variable>(
            std::move(l),
            std::move(r)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));

        auto scope = engine.module_scopes["main"].get();
        BOOST_TEST((scope->find_symbol("foo") != nullptr));
    }

    BOOST_AUTO_TEST_CASE (infer_constant_definition)
    {
        BOOST_TEST_MESSAGE("Testing inference of constant definition");

        auto l = std::make_unique<Identifier>("foo");
        auto r = make_expression<Integer>(69);
        std::unique_ptr<ASTNode> node = make_statement<Constant>(
            std::move(l),
            std::move(r)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));

        auto scope = engine.module_scopes["main"].get();
        BOOST_TEST((scope->find_symbol("foo") != nullptr));
    }

    BOOST_AUTO_TEST_CASE (infer_type_declaration)
    {
        BOOST_TEST_MESSAGE("Testing inference of type declaration");

        auto l = std::make_unique<Identifier>("foo");
        auto r = std::make_unique<BuiltinType>(BasicType::Integer);

        std::unique_ptr<ASTNode> node = make_statement<TypeDeclaration>(
            std::move(l),
            std::move(r)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));

        auto scope = engine.module_scopes["main"].get();
        BOOST_TEST((scope->find_symbol("foo") != nullptr));
    }

    BOOST_AUTO_TEST_CASE (infer_type_alias)
    {
        BOOST_TEST_MESSAGE("Testing inference of type alias");

        auto l = std::make_unique<Identifier>("foo");
        auto r = std::make_unique<BuiltinType>(BasicType::Integer);

        std::unique_ptr<ASTNode> node = make_statement<Alias>(
            std::move(l),
            std::move(r)
        );

        node->visit(&engine.visitor);

        auto new_type = engine.mapper.get_type_for("foo");
        auto as_simple = util::get_if<SimpleType>(&new_type.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_array_type_alias)
    {
        BOOST_TEST_MESSAGE("Testing inference of type alias for array");

        auto l = std::make_unique<Identifier>("foo");
        auto rid = std::make_unique<BuiltinType>(BasicType::Integer);
        auto rexp = make_expression<Integer>(42);
        auto r = std::make_unique<VectorTypename>(std::move(rid), std::move(rexp));

        std::unique_ptr<ASTNode> node = make_statement<Alias>(
            std::move(l),
            std::move(r)
        );

        node->visit(&engine.visitor);

        auto new_type = engine.mapper.get_type_for("foo");
        auto as_array = util::get_if<ArrayType>(&new_type.type());
        BOOST_TEST((as_array != nullptr));
        auto as_simple = util::get_if<SimpleType>(&as_array->base_type->type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_generic_type_alias)
    {
        BOOST_TEST_MESSAGE("Testing inference of alias for generic type");

        auto l = std::make_unique<Identifier>("foo");
        auto rgen_id = std::make_unique<Identifier>("list");
        auto rgen_v = std::vector<std::unique_ptr<Typename>>();
        auto rt = std::make_unique<BuiltinType>(BasicType::Integer);
        rgen_v.emplace_back(std::move(rt));
        auto r = std::make_unique<GenericTypename>(std::move(rgen_id), rgen_v);

        std::unique_ptr<ASTNode> node = make_statement<Alias>(
            std::move(l),
            std::move(r)
        );

        node->visit(&engine.visitor);

        auto new_type = engine.mapper.get_type_for("foo");
        auto as_generic = util::get_if<GenericType>(&new_type.type());
        BOOST_TEST((as_generic != nullptr));
        auto as_simple = util::get_if<SimpleType>(&as_generic->concrete_types[0]->type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_enum_declaration)
    {
        BOOST_TEST_MESSAGE("Testing inference of enum declaration");

        std::vector<std::string> syms { "foo", "bar", "baz" };
        auto sl = std::make_unique<SymbolList>(syms);

        auto node = std::make_unique<Enum>(
            std::move(std::make_unique<Identifier>("En")),
            std::move(sl)
        );

        node->visit(&engine.visitor);

        auto type = engine.mapper.get_type_for("En");
        auto as_enum = util::get_if<EnumType>(&type.type());
        BOOST_TEST((as_enum != nullptr));
        BOOST_TEST((as_enum->name == "En"));
        BOOST_TEST((as_enum->values.size() == 3));
        BOOST_TEST((as_enum->values[0] == "foo"));
    }

    BOOST_AUTO_TEST_CASE (infer_optional)
    {
        BOOST_TEST_MESSAGE("Testing inference of optional");

        auto t = std::make_unique<BuiltinType>(BasicType::Integer);

        std::unique_ptr<ASTNode> node = std::make_unique<Optional>(
            std::move(t)
        );

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_optional = util::get_if<OptionalType>(&inferred.type());
        BOOST_TEST((as_optional != nullptr));
        
        auto contained = *(as_optional->contained_type);
        auto contained_as_simple = util::get_if<SimpleType>(&contained.type());
        BOOST_TEST((contained_as_simple != nullptr));
        BOOST_TEST((contained_as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_variant)
    {
        BOOST_TEST_MESSAGE("Testing inference of variant");

        std::vector<std::unique_ptr<Typename>> ts;

        auto t = std::make_unique<BuiltinType>(BasicType::Integer);
        ts.emplace_back(std::move(t));

        auto tid = std::make_unique<Identifier>("string");
        t = std::make_unique<BuiltinType>(BasicType::String);
        ts.emplace_back(std::move(t));

        std::unique_ptr<ASTNode> node = std::make_unique<Variant>(ts);

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_variant = util::get_if<VariantType>(&inferred.type());
        BOOST_TEST((as_variant != nullptr));

        BOOST_TEST((as_variant->types.size() == 2));
        auto variant_type0 = *(as_variant->types[0].get());
        auto variant_type1 = *(as_variant->types[1].get());
        
        auto t0_as_simple = util::get_if<SimpleType>(&variant_type0.type());
        BOOST_TEST((t0_as_simple != nullptr));
        BOOST_TEST((t0_as_simple->type == BasicType::Integer));

        auto t1_as_simple = util::get_if<SimpleType>(&variant_type1.type());
        BOOST_TEST((t1_as_simple != nullptr));
        BOOST_TEST((t1_as_simple->type == BasicType::String));
    }

    BOOST_AUTO_TEST_CASE (infer_function_type)
    {
        BOOST_TEST_MESSAGE("Testing inference of simple function definition");

        std::vector<std::unique_ptr<Statement>> empty_body;
        auto body = make_statement<Block>(empty_body);
        std::vector<std::unique_ptr<Condition>> empty_cond;

        auto node = make_statement<Def>(
            rhea::ast::FunctionClass::Basic,
            "foo",
            nullptr,
            nullptr,
            empty_cond,
            std::move(body)
        );

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto scope = engine.module_scopes["main"]->root.get();
        BOOST_TEST((scope != nullptr));
        BOOST_TEST((scope->find_symbol("foo@" + std::to_string(reinterpret_cast<std::uintptr_t>(node.get()))) != nullptr));
        auto defs = scope->find_function_definitions("foo");
        BOOST_TEST((defs.first != defs.second));
        auto d = defs.first->second;
        BOOST_TEST((d == node.get()));
    }

    BOOST_AUTO_TEST_CASE (infer_return_type)
    {
        BOOST_TEST_MESSAGE("Testing inference of return statement");

        auto rvalue = make_expression<Integer>(42);

        auto node = make_statement<Return>(std::move(rvalue));

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_throw_type)
    {
        BOOST_TEST_MESSAGE("Testing inference of throw statement");

        auto rvalue = make_expression<Integer>(42);

        auto node = make_statement<Throw>(std::move(rvalue));

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_structure_type)
    {
        BOOST_TEST_MESSAGE("Testing inference of structure definition");

        auto name = std::make_unique<Identifier>("St");
        auto ftype = std::make_unique<BuiltinType>(BasicType::Integer);
        auto ftypepair = std::make_unique<TypePair>("foo", std::move(ftype));
        std::vector<std::unique_ptr<TypePair>> fields;
        fields.emplace_back(std::move(ftypepair));

        auto node = make_statement<Structure>(std::move(name), fields);

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_struct = util::get_if<StructureType>(&inferred.type());
        BOOST_TEST((as_struct != nullptr));
        BOOST_TEST((as_struct->name == "St"));
        BOOST_TEST((as_struct->fields.size() == 1));
        BOOST_TEST_MESSAGE(canonical_name(inferred));
        BOOST_TEST((canonical_name(inferred) == "Structure(St|integer)"));
    }

    BOOST_AUTO_TEST_CASE (infer_list_expression)
    {
        BOOST_TEST_MESSAGE("Testing inference of list expression");

        auto expr1 = make_expression<Integer>(42);
        auto expr2 = make_expression<Integer>(69);
        std::vector<std::unique_ptr<Expression>> items;
        items.emplace_back(std::move(expr1)),
        items.emplace_back(std::move(expr2));

        auto node = make_expression<List>(items);

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_list = util::get_if<ListType>(&inferred.type());
        BOOST_TEST((as_list != nullptr));
        
        auto base_type_as_simple = util::get_if<SimpleType>(&(as_list->base_type->type()));
        BOOST_TEST((base_type_as_simple != nullptr));
        BOOST_TEST((base_type_as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_array_expression)
    {
        BOOST_TEST_MESSAGE("Testing inference of array expression");

        auto expr1 = make_expression<Integer>(42);
        auto expr2 = make_expression<Integer>(69);
        std::vector<std::unique_ptr<Expression>> items;
        items.emplace_back(std::move(expr1)),
        items.emplace_back(std::move(expr2));

        auto node = make_expression<Array>(items);

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_array = util::get_if<ArrayType>(&inferred.type());
        BOOST_TEST((as_array != nullptr));
        BOOST_TEST((as_array->size == 2));
        
        auto base_type_as_simple = util::get_if<SimpleType>(&(as_array->base_type->type()));
        BOOST_TEST((base_type_as_simple != nullptr));
        BOOST_TEST((base_type_as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_tuple_expression)
    {
        BOOST_TEST_MESSAGE("Testing inference of tuple expression");

        auto expr1 = make_expression<Integer>(42);
        auto expr2 = make_expression<Symbol>("foo");
        std::vector<std::unique_ptr<Expression>> items;
        items.emplace_back(std::move(expr1)),
        items.emplace_back(std::move(expr2));

        auto node = make_expression<Tuple>(items);

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_tuple = util::get_if<TupleType>(&inferred.type());
        BOOST_TEST((as_tuple != nullptr));
        BOOST_TEST((as_tuple->contained_types.size() == 2));

        auto item_as_simple = util::get_if<SimpleType>(&as_tuple->contained_types[0]->type());
        BOOST_TEST((item_as_simple != nullptr));
        BOOST_TEST((item_as_simple->type == BasicType::Integer));
        item_as_simple = util::get_if<SimpleType>(&as_tuple->contained_types[1]->type());
        BOOST_TEST((item_as_simple != nullptr));
        BOOST_TEST((item_as_simple->type == BasicType::Symbol));
    }

    BOOST_AUTO_TEST_CASE (infer_dictionary_expression)
    {
        BOOST_TEST_MESSAGE("Testing inference of dictionary expression");

        auto key = std::make_unique<Symbol>("foo");
        auto value = make_expression<Integer>(42);
        auto kv_pair = std::make_unique<DictionaryEntry>(std::move(key), std::move(value));
        
        std::vector<std::unique_ptr<DictionaryEntry>> items;
        items.emplace_back(std::move(kv_pair));

        auto node = make_expression<Dictionary>(items);

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_dict = util::get_if<DictionaryType>(&inferred.type());
        BOOST_TEST((as_dict != nullptr));
        auto contained = as_dict->base_type;
        auto contained_as_simple = util::get_if<SimpleType>(&contained->type());
        BOOST_TEST((contained_as_simple != nullptr));
        BOOST_TEST((contained_as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_extern_declaration)
    {
        BOOST_TEST_MESSAGE("Testing inference of extern declaration");

        auto rt = std::make_unique<BuiltinType>(BasicType::Integer);
        auto at = std::make_unique<BuiltinType>(BasicType::String);
        std::vector<std::unique_ptr<Typename>> ats;
        ats.emplace_back(std::move(at));

        auto node = make_statement<Extern>("foo", ats, std::move(rt));

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto scope = engine.module_scopes["main"]->root.get();
        BOOST_TEST((scope != nullptr));
        BOOST_TEST((scope->find_symbol("foo@extern") != nullptr));
        
        auto inferred = engine.inferred_types[node.get()]();
        auto as_function = util::get_if<rhea::types::FunctionType>(&inferred.type());
        BOOST_TEST((as_function != nullptr));
        auto return_as_simple = util::get_if<SimpleType>(&(as_function->return_type->type()));
        BOOST_TEST((return_as_simple != nullptr));
        BOOST_TEST((return_as_simple->type == BasicType::Integer));
    }

    BOOST_AUTO_TEST_CASE (infer_module_definition)
    {
        BOOST_TEST_MESSAGE("Testing inference of module definition");

        auto id = make_identifier<Identifier>("mymodule");
        auto name = std::make_unique<ModuleName>(std::move(id));

        auto node = make_statement<ModuleDef>(std::move(name));

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto scope = engine.module_scopes["mymodule"].get();
        BOOST_TEST((scope != nullptr));
        BOOST_TEST((scope->name == "mymodule"));
    }

    BOOST_AUTO_TEST_CASE (infer_export_declaration)
    {
        BOOST_TEST_MESSAGE("Testing inference of export declaration");

        auto id = std::make_unique<Identifier>("foo");
        std::vector<std::unique_ptr<Identifier>> ids;
        ids.emplace_back(std::move(id));

        auto node = make_statement<Export>(ids);

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto scope = engine.module_scopes["main"].get();
        BOOST_TEST((scope != nullptr));
        auto pos = std::find(std::begin(scope->exports), std::end(scope->exports), std::string("foo"));
        BOOST_TEST((pos != std::end(scope->exports)));
    }

    BOOST_AUTO_TEST_CASE (infer_use_declaration)
    {
        BOOST_TEST_MESSAGE("Testing inference of use declaration");

        auto id = make_identifier<Identifier>("mymodule");
        auto mn = std::make_unique<ModuleName>(std::move(id));

        auto node = make_statement<Use>(std::move(mn));

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto scope = engine.module_scopes["mymodule"].get();
        BOOST_TEST((scope != nullptr));
    }

    BOOST_AUTO_TEST_CASE (infer_import_declaration)
    {
        BOOST_TEST_MESSAGE("Testing inference of import statement");

        // Set up a fake module.
        auto mod_id = make_identifier<Identifier>("mymodule");
        auto mod_name = std::make_unique<ModuleName>(std::move(mod_id));
        auto mod_node = make_statement<ModuleDef>(std::move(mod_name));
        mod_node->visit(&engine.visitor);

        // Add an export.
        auto exp_id = std::make_unique<Identifier>("foo");
        std::vector<std::unique_ptr<Identifier>> exp_ids;
        exp_ids.emplace_back(std::move(exp_id));
        auto exp_node = make_statement<Export>(exp_ids);
        exp_node->visit(&engine.visitor);

        // We also have to give it something *to* export.
        auto sym_id = std::make_unique<Identifier>("foo");
        auto sym_expr = make_expression<Integer>(42);
        auto sym_node = make_statement<Constant>(std::move(sym_id), std::move(sym_expr));
        sym_node->visit(&engine.visitor);

        // Now we can go back to the main module and import.
        engine.visitor.module_scope = engine.module_scopes["main"].get();
        auto id = make_identifier<Identifier>("mymodule");
        auto mn = std::make_unique<ModuleName>(std::move(id));
        auto sym = std::make_unique<Identifier>("foo");
        std::vector<std::unique_ptr<Identifier>> syms;
        syms.emplace_back(std::move(sym));

        auto node = make_statement<Import>(syms, std::move(mn));

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto scope = engine.module_scopes["mymodule"].get();
        BOOST_TEST((scope != nullptr));

        auto imported_symbol = engine.module_scopes["main"]->find_symbol("foo");
        BOOST_TEST((imported_symbol != nullptr));
    }

    BOOST_AUTO_TEST_CASE (infer_concept_definition)
    {
        BOOST_TEST_MESSAGE("Testing inference of concept definition");

        std::vector<ConceptCheck> ccs;
        auto node = make_statement<Concept>("Testable", "Ty", ccs);

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto new_type = engine.mapper.get_type_for("Testable");
        auto as_concept = util::get_if<ConceptType>(&new_type.type());
        BOOST_TEST((as_concept != nullptr));
        BOOST_TEST((as_concept->name == "Testable"));
    }

    BOOST_AUTO_TEST_CASE (infer_fully_qualified_identifier)
    {
        BOOST_TEST_MESSAGE("Testing inference of fully-qualified identifier");

        // Set up a fake module.
        auto mod_id = make_identifier<Identifier>("mymodule");
        auto mod_name = std::make_unique<ModuleName>(std::move(mod_id));
        auto mod_node = make_statement<ModuleDef>(std::move(mod_name));
        mod_node->visit(&engine.visitor);

        // Add an export.
        auto exp_id = std::make_unique<Identifier>("foo");
        std::vector<std::unique_ptr<Identifier>> exp_ids;
        exp_ids.emplace_back(std::move(exp_id));
        auto exp_node = make_statement<Export>(exp_ids);
        exp_node->visit(&engine.visitor);

        // We also have to give it something *to* export.
        auto sym_id = std::make_unique<Identifier>("foo");
        auto sym_expr = make_expression<Integer>(42);
        auto sym_node = make_statement<Constant>(std::move(sym_id), std::move(sym_expr));
        sym_node->visit(&engine.visitor);

        // Now we can go back to the main module and use.
        engine.visitor.module_scope = engine.module_scopes["main"].get();
        auto use_id = make_identifier<Identifier>("mymodule");
        auto use_mn = std::make_unique<ModuleName>(std::move(use_id));
        auto use_node = make_statement<Use>(std::move(use_mn));
        use_node->visit(&engine.visitor);
       
        auto fq_mod_id = std::make_unique<Identifier>("mymodule");
        auto fq_id = std::make_unique<Identifier>("foo");
        std::vector<std::unique_ptr<Identifier>> fq_parts;
        fq_parts.emplace_back(std::move(fq_mod_id));
        fq_parts.emplace_back(std::move(fq_id));
        
        auto node = make_expression<FullyQualified>(fq_parts);

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));       
    }

    BOOST_AUTO_TEST_CASE (infer_relative_identifier)
    {
        BOOST_TEST_MESSAGE("Testing inference of module-relative identifier");

        // Set up a fake module; note that it has to be a submodule this time.
        auto mod_part1 = std::make_unique<Identifier>("main");
        auto mod_part2 = std::make_unique<Identifier>("mymodule");
        std::vector<std::unique_ptr<Identifier>> mod_parts;
        mod_parts.emplace_back(std::move(mod_part1));
        mod_parts.emplace_back(std::move(mod_part2));
        auto mod_id = make_identifier<FullyQualified>(mod_parts);
        auto mod_name = std::make_unique<ModuleName>(std::move(mod_id));
        auto mod_node = make_statement<ModuleDef>(std::move(mod_name));
        mod_node->visit(&engine.visitor);

        // Add an export.
        auto exp_id = std::make_unique<Identifier>("foo");
        std::vector<std::unique_ptr<Identifier>> exp_ids;
        exp_ids.emplace_back(std::move(exp_id));
        auto exp_node = make_statement<Export>(exp_ids);
        exp_node->visit(&engine.visitor);

        // We also have to give it something *to* export.
        auto sym_id = std::make_unique<Identifier>("foo");
        auto sym_expr = make_expression<Integer>(42);
        auto sym_node = make_statement<Constant>(std::move(sym_id), std::move(sym_expr));
        sym_node->visit(&engine.visitor);

        // Now we can go back to the main module and use.
        engine.visitor.module_scope = engine.module_scopes["main"].get();
        auto use_id = make_identifier<Identifier>("mymodule");
        auto use_mn = std::make_unique<ModuleName>(std::move(use_id));
        auto use_node = make_statement<Use>(std::move(use_mn));
        use_node->visit(&engine.visitor);
       
        auto rel_mod_id = std::make_unique<Identifier>("mymodule");
        auto rel_id = std::make_unique<Identifier>("foo");
        std::vector<std::unique_ptr<Identifier>> rel_parts;
        rel_parts.emplace_back(std::move(rel_mod_id));
        rel_parts.emplace_back(std::move(rel_id));
        auto fq = std::make_unique<FullyQualified>(rel_parts);
        
        auto node = make_expression<RelativeIdentifier>(std::move(fq));

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_simple = util::get_if<SimpleType>(&inferred.type());
        BOOST_TEST((as_simple != nullptr));
        BOOST_TEST((as_simple->type == BasicType::Integer));       
    }

    BOOST_AUTO_TEST_CASE (infer_function_call)
    {
        BOOST_TEST_MESSAGE("Testing inference of function call expression");

        // Add a definition for us to find.
        std::vector<std::unique_ptr<Statement>> empty_body;
        auto body = make_statement<Block>(empty_body);
        std::vector<std::unique_ptr<Condition>> empty_cond;

        auto def_node = make_statement<Def>(
            rhea::ast::FunctionClass::Basic,
            "foo",
            nullptr,
            nullptr,
            empty_cond,
            std::move(body)
        );

        def_node->visit(&engine.visitor);

        // Now create the call.
        std::vector<std::unique_ptr<Expression>> args;
        auto fname = make_expression<Identifier>("foo");

        auto node = make_expression<Call>(std::move(fname), args);

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto inferred = engine.inferred_types[node.get()]();
        auto as_nothing = util::get_if<NothingType>(&inferred.type());
        BOOST_TEST((as_nothing != nullptr));
    }

    BOOST_AUTO_TEST_CASE (infer_generic_function_def)
    {
        BOOST_TEST_MESSAGE("Testing inference of generic function definition");

        std::vector<std::unique_ptr<Statement>> empty_body;
        auto body = make_statement<Block>(empty_body);
        std::vector<std::unique_ptr<Condition>> empty_cond;
        std::vector<GenericMatch> empty_matches;

        auto node = make_statement<GenericDef>(
            rhea::ast::FunctionClass::Basic,
            "foo",
            empty_matches,
            nullptr,
            nullptr,
            empty_cond,
            std::move(body)
        );

        BOOST_TEST((node != nullptr));

        node->visit(&engine.visitor);

        auto scope = engine.module_scopes["main"]->root.get();
        BOOST_TEST((scope != nullptr));
        BOOST_TEST((scope->find_symbol("foo@" + std::to_string(reinterpret_cast<std::uintptr_t>(node.get()))) != nullptr));
        auto defs = scope->find_function_definitions("foo");
        BOOST_TEST((defs.first != defs.second));
        auto d = defs.first->second;
        BOOST_TEST((d == node.get()));
    }

    BOOST_AUTO_TEST_SUITE_END()
}