#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/test/data/monomorphic.hpp>
#include <boost/algorithm/string/split.hpp>

#include <string>
#include <vector>
#include <memory>
#include <exception>

#include "../../include/types/types.hpp"
#include "../../include/types/to_string.hpp"
#include "../../include/types/name_mangle.hpp"

namespace data = boost::unit_test::data;
namespace util = rhea::util;

namespace {
    using namespace rhea::types;

    // Datasets

    // Test cases
    BOOST_AUTO_TEST_SUITE (Name_mangler)

    BOOST_AUTO_TEST_CASE (simple_function_no_args)
    {
        BOOST_TEST_MESSAGE("Testing mangling of simple function without arguments or specified return type");
        FunctionType ft {};

        auto mangled = mangle_function_name("foo", ft);
        BOOST_TEST(mangled == "_Rf3foov0");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_any_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of any type");
        AnyType t {};

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "a");
    }
    
    BOOST_AUTO_TEST_CASE (internal_mangle_nothing_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of nothing type");
        NothingType t {};

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "v");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_byte_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of byte type");
        SimpleType t { BasicType::Byte, true, true };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "c");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_ubyte_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of unsigned byte type");
        SimpleType t { BasicType::UnsignedByte, true, true };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "C");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_integer_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of integer type");
        SimpleType t { BasicType::Integer, true, true };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "i");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_uinteger_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of unsigned integer type");
        SimpleType t { BasicType::UnsignedInteger, true, true };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "I");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_long_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of long type");
        SimpleType t { BasicType::Long, true, true };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "l");
    }
    
    BOOST_AUTO_TEST_CASE (internal_mangle_ulong_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of unsigned long type");
        SimpleType t { BasicType::UnsignedLong, true, true };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "L");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_double_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of double type");
        SimpleType t { BasicType::Double, true, false };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "Dd");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_float_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of float type");
        SimpleType t { BasicType::Float, true, false };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "Df");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_boolean_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of boolean type");
        SimpleType t { BasicType::Boolean, false, false };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "b");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_symbol_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of symbol type");
        SimpleType t { BasicType::Symbol, false, false };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "Sy");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_string_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of string type");
        SimpleType t { BasicType::String, false, false };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "s");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_optional_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of optional type");
        SimpleType base { BasicType::Integer, true, true };
        OptionalType t { std::make_shared<TypeInfo>(base) };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "Opi");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_variant_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of variant type");
        SimpleType base1 { BasicType::Integer, true, true };
        SimpleType base2 { BasicType::String };
        std::vector<std::shared_ptr<TypeInfo>> types;
        types.push_back(std::make_shared<TypeInfo>(base1));
        types.push_back(std::make_shared<TypeInfo>(base2));
        VariantType t { types };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "V2_is");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_enum_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of enum type");
        std::vector<std::string> values { "foo", "bar", "baz" };
        EnumType t { "En", values };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "E2En");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_structure_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of structure type");
        SimpleType base1 { BasicType::Integer, true, true };
        SimpleType base2 { BasicType::String };
        std::vector<std::pair<std::string, std::shared_ptr<TypeInfo>>> types;
        types.push_back({ "foo", std::make_shared<TypeInfo>(base1) });
        types.push_back({ "bar", std::make_shared<TypeInfo>(base2) });
        StructureType t { "St", types };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "Ss2St");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_reference_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of reference type");
        SimpleType base { BasicType::String };
        ReferenceType t { std::make_shared<TypeInfo>(base) };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "rs");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_pointer_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of pointer type");
        SimpleType base { BasicType::String };
        PointerType t { std::make_shared<TypeInfo>(base) };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "ps");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_array_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of array type");
        SimpleType base { BasicType::String };
        ArrayType t { std::make_shared<TypeInfo>(base), 42u };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "A42_s");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_list_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of list type");
        SimpleType base { BasicType::String };
        ListType t { std::make_shared<TypeInfo>(base) };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "Sls");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_dictionary_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of dictionary type");
        SimpleType base { BasicType::String };
        DictionaryType t { std::make_shared<TypeInfo>(base) };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "Sds");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_tuple_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of tuple type");
        SimpleType base1 { BasicType::Integer, true, true };
        SimpleType base2 { BasicType::String };
        std::vector<std::shared_ptr<TypeInfo>> types;
        types.push_back(std::make_shared<TypeInfo>(base1));
        types.push_back(std::make_shared<TypeInfo>(base2));
        TupleType t { types };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "T2_is");
    }

    BOOST_AUTO_TEST_CASE (internal_mangle_generic_type)
    {
        BOOST_TEST_MESSAGE("Testing mangling of generic type");
        SimpleType gen1 { BasicType::Integer, true, true };
        SimpleType gen2 { BasicType::String };
        std::vector<std::shared_ptr<TypeInfo>> types;
        types.push_back(std::make_shared<TypeInfo>(gen1));
        types.push_back(std::make_shared<TypeInfo>(gen2));
        StructureType base { "St", {} };
        GenericType t { std::make_shared<TypeInfo>(base), types };

        auto mangled = internal::name_mangle_visitor()(t);
        BOOST_TEST(mangled == "GSs2St2_is");
    }

    BOOST_AUTO_TEST_SUITE_END ()
}