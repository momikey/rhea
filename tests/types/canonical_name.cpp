#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/test/data/monomorphic.hpp>

#include <string>
#include <vector>
#include <memory>
#include <utility>

#include "../../include/types/types.hpp"
#include "../../include/types/canonical_name.hpp"

namespace data = boost::unit_test::data;
namespace ast = rhea::ast;
namespace types = rhea::types;
namespace util = rhea::util;

namespace {
    using types::TypeInfo;
    using types::SimpleType;
    using types::BasicType;

    // Datasets

    // Test cases
    BOOST_AUTO_TEST_SUITE (Canonical_typenames)

    BOOST_AUTO_TEST_CASE (canonical_typename_any)
    {
        TypeInfo ti { types::AnyType() };

        BOOST_TEST_MESSAGE("Testing canonicalization of type any");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "any"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_nothing)
    {
        TypeInfo ti { types::NothingType() };

        BOOST_TEST_MESSAGE("Testing canonicalization of type nothing");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "nothing"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_integer)
    {
        TypeInfo ti { SimpleType(BasicType::Integer, true, true) };

        BOOST_TEST_MESSAGE("Testing canonicalization of type integer");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "integer"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_double)
    {
        TypeInfo ti { SimpleType(BasicType::Double, true, false) };

        BOOST_TEST_MESSAGE("Testing canonicalization of type double");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "double"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_symbol)
    {
        TypeInfo ti { SimpleType(BasicType::Symbol, false, false) };

        BOOST_TEST_MESSAGE("Testing canonicalization of type symbol");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "symbol"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_optional)
    {
        auto contained = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        types::OptionalType ot { contained };
        TypeInfo ti { ot };

        BOOST_TEST_MESSAGE("Testing canonicalization of optional string type");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "Optional(string)"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_variant)
    {
        auto contained1 = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        auto contained2 = std::make_shared<TypeInfo>(SimpleType(BasicType::Integer, true, true));
        std::vector<std::shared_ptr<TypeInfo>> contained { contained1, contained2 };
        types::VariantType vt { contained };
        TypeInfo ti { vt };

        BOOST_TEST_MESSAGE("Testing canonicalization of variant type");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "Variant(string,integer)"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_enum)
    {
        std::vector<std::string> values { "foo", "bar" };
        types::EnumType et { "En", values };
        TypeInfo ti { et };

        BOOST_TEST_MESSAGE("Testing canonicalization of enum type");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "Enum(En)"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_reference)
    {
        auto contained = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        types::ReferenceType rt { contained };
        TypeInfo ti { rt };

        BOOST_TEST_MESSAGE("Testing canonicalization of reference to string");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "Reference(string)"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_pointer)
    {
        auto contained = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        types::PointerType pt { contained };
        TypeInfo ti { pt };

        BOOST_TEST_MESSAGE("Testing canonicalization of reference to string");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "Pointer(string)"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_array)
    {
        auto contained = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        types::ArrayType arrt { contained, 42u };
        TypeInfo ti { arrt };

        BOOST_TEST_MESSAGE("Testing canonicalization of array of strings");

        auto result = types::canonical_name(ti);

        BOOST_TEST_MESSAGE(result);
        BOOST_TEST((result == "string[42]"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_list)
    {
        auto contained = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        types::ListType lt { contained };
        TypeInfo ti { lt };

        BOOST_TEST_MESSAGE("Testing canonicalization of list of strings");

        auto result = types::canonical_name(ti);

        BOOST_TEST_MESSAGE(result);
        BOOST_TEST((result == "List(string)"));
    }
    
    BOOST_AUTO_TEST_CASE (canonical_typename_dictionary)
    {
        auto contained = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        types::DictionaryType dt { contained };
        TypeInfo ti { dt };

        BOOST_TEST_MESSAGE("Testing canonicalization of dictionary of strings");

        auto result = types::canonical_name(ti);

        BOOST_TEST_MESSAGE(result);
        BOOST_TEST((result == "Dictionary(string)"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_generic)
    {
        auto contained1 = std::make_shared<TypeInfo>(SimpleType(BasicType::Byte, true, true));
        std::vector<std::shared_ptr<TypeInfo>> contained { contained1 };
        auto base = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        types::GenericType gt { base, contained };
        TypeInfo ti { gt };

        BOOST_TEST_MESSAGE("Testing canonicalization of generic type");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "string<byte>"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_structure)
    {
        auto contained1 = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        auto contained2 = std::make_shared<TypeInfo>(SimpleType(BasicType::Integer, true, true));
        std::vector<std::pair<std::string, std::shared_ptr<TypeInfo>>> contained;
        contained.emplace_back(std::make_pair("foo", contained1));
        contained.emplace_back(std::make_pair("bar", contained2));
        types::StructureType strt { "S", contained };
        TypeInfo ti { strt };

        BOOST_TEST_MESSAGE("Testing canonicalization of structure type");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "Structure(S|string,integer)"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_tuple)
    {
        auto contained1 = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        auto contained2 = std::make_shared<TypeInfo>(SimpleType(BasicType::Integer, true, true));
        std::vector<std::shared_ptr<TypeInfo>> contained;
        contained.emplace_back(contained1);
        contained.emplace_back(contained2);
        types::TupleType tt { contained };
        TypeInfo ti { tt };

        BOOST_TEST_MESSAGE("Testing canonicalization of tuple type");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "<string,integer>"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_function)
    {
        auto contained1 = std::make_shared<TypeInfo>(SimpleType(BasicType::String));
        auto contained2 = std::make_shared<TypeInfo>(SimpleType(BasicType::Integer, true, true));
        std::vector<std::pair<std::string, std::shared_ptr<TypeInfo>>> contained;
        contained.emplace_back(std::make_pair("foo", contained1));
        contained.emplace_back(std::make_pair("bar", contained2));

        auto ret = std::make_shared<TypeInfo>(SimpleType(BasicType::Boolean));
        types::FunctionType ft { contained, ret, rhea::ast::FunctionClass::Basic };
        TypeInfo ti { ft };

        BOOST_TEST_MESSAGE("Testing canonicalization of function type");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "<string,integer> -> boolean"));
    }

    BOOST_AUTO_TEST_CASE (canonical_typename_concept)
    {
        types::ConceptType c { "Testable" };
        TypeInfo ti { c };

        BOOST_TEST_MESSAGE("Testing canonicalization of concept type");

        auto result = types::canonical_name(ti);

        BOOST_TEST((result == "Concept(Testable)"));
    }

    BOOST_AUTO_TEST_SUITE_END ()
}