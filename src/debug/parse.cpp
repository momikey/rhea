#include <iostream>
#include <string>

#include <tao/pegtl/analyze.hpp>

#include "grammar/expression.hpp"
#include "debug/parse_tree.hpp"

int main()
{
    const std::size_t issues_found = tao::pegtl::analyze<rhea::grammar::type_name>();

    std::string input;

    while (std::getline(std::cin, input))
    {
        auto in = rhea::debug::input_from_string(input);
        auto tree = rhea::debug::parse<rhea::ast::parser_node>(*in);
        rhea::debug::print_partial_tree(std::cout, tree.get());
    }

    return 0;
}
