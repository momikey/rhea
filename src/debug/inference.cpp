#include <iostream>
#include <string>

#include "debug/parse_tree.hpp"
#include "debug/build_ast.hpp"
#include "debug/type_inference.hpp"

int main()
{
    std::string input;
    std::string collected_input;

    std::cout << "-- Multiline input: use ';' on a line by itself to terminate input --\n";

    while (std::getline(std::cin, input))
    {
        if (input != ";")
        {
            collected_input += input;
        }
        else
        {
            auto in = rhea::debug::input_from_string(collected_input);
            auto tree = rhea::debug::parse<rhea::ast::parser_node>(*in);
            
            if (tree)
            {
                auto ast = rhea::debug::build_ast(tree.get());
                std::cout << "AST: " << ast->to_string() << '\n';

                auto inf_map = rhea::debug::build_inference_map(ast.get());
                rhea::debug::dump_inference_map(std::cout, inf_map.get());
                std::cout << '\n';
            }
            else
            {
                std::cout << "Parse error\n";
            }

            collected_input = "";
        }
    }

    return 0;
}
