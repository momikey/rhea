#include "inference/visitor.hpp"
#include "inference/engine.hpp"

#include <iostream>

namespace rhea { namespace inference {
    using namespace rhea::ast;
    using namespace rhea::types;
    using util::any;

    using type_vector = std::vector<TypeInfo>;

    /*
     * Note that there is no need for a return value in any of these.
     * 
     * Also, the inference engine's type map requires us to store pointers to the base
     * AST node class. As this visitor is (or *should be*) the only way those pointers
     * are created, we can guarantee that they point to the correct derived class, and
     * thus we can use static_cast instead of the much slower dynamic_cast.
     */

    any InferenceVisitor::visit(Program* n)
    {
        // TOOD: At the moment, we don't have to do too much with programs. All we need
        // is a simple iteration through the statements. In the future, we'll have to deal
        // with the other stuff.
        std::for_each(n->children.begin(), n->children.end(), 
            [this](auto& e) { e->visit(this); });
        
        return {};
    }

    any InferenceVisitor::visit(Module* n)
    {
        // TOSO: At present, there's essentially no difference between a program and
        // a module, at least as far as this phase is concerned. The `module` statement
        // node will handle the state transition. Thus, all we need to do is iterate
        // over the contained statements.
        std::for_each(n->children.begin(), n->children.end(),
            [this](auto& e) { e->visit(this); });

        return {};
    }

    any InferenceVisitor::visit(Boolean* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::Boolean, false); } };
        return {};
    }

    any InferenceVisitor::visit(Integer* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::Integer, true, true); } };
        return {};
    }

    any InferenceVisitor::visit(Byte* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::Byte, true, true); } };
        return {};
    }

    any InferenceVisitor::visit(Long* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::Long, true, true); } };
        return {};
    }

    any InferenceVisitor::visit(UnsignedInteger* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::UnsignedInteger, true, true); } };
        return {};
    }

    any InferenceVisitor::visit(UnsignedByte* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::UnsignedByte, true, true); } };
        return {};
    }

    any InferenceVisitor::visit(UnsignedLong* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::UnsignedLong, true, true); } };
        return {};
    }

    any InferenceVisitor::visit(Float* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::Float, true, false); } };
        return {};
    }

    any InferenceVisitor::visit(Double* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::Double, true, false); } };
        return {};
    }

    any InferenceVisitor::visit(Symbol* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::Symbol, false); } };
        return {};
    }

    any InferenceVisitor::visit(String* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return SimpleType(BasicType::String, false); } };
        return {};
    }

    any InferenceVisitor::visit(Nothing* n)
    {
        engine->inferred_types[n] =
            InferredType { [](TypeEngine* e, ASTNode* node, InferenceState state)
                { return NothingType(); } };
        return {};
    }

    any InferenceVisitor::visit(Identifier* n)
    {
        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Identifier*>(node);

                    auto symbol = state.scope->find_symbol(derived->name);
                    if (symbol != nullptr)
                    {
                        return e->inferred_types[symbol]();
                    }
                    else
                    {
                        // TODO: Symbol not found. Is this always an error condition?
                        return TypeInfo { UnknownType() };
                    }
                },
            engine, n, { module_scope->current_scope }
        };
        
        return {};
    }

    any InferenceVisitor::visit(FullyQualified* n)
    {
        for (auto& c : n->children)
        {
            c->visit(this);
        }

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<FullyQualified*>(node);

                    std::vector<std::string> strs;
                    std::transform(
                        derived->children.begin(),
                        derived->children.end(),
                        std::back_inserter(strs),
                        [](const auto& item) { return item->name; }
                    );

                    // Get the module name, which is created by combining each
                    // part of the fully-qualified identifier except the last.
                    auto module_name = fmt::format("{}", fmt::join(
                        strs.begin(),
                        strs.end() - 1,
                        ":"
                    ));

                    if (e->module_scopes.count(module_name) > 0)
                    {
                        // This module has been added at some point during the compile,
                        // so now we check to ensure that the current module uses it.
                        auto containing_module = e->module_scopes[module_name].get();
                        auto this_module = state.scope->containing_module;
                        auto pos = std::find(
                            this_module->imports.begin(),
                            this_module->imports.end(),
                            containing_module
                        );

                        if (pos != this_module->imports.end())
                        {
                            // Module was the target of a use declaration in this module.
                            auto sym = containing_module->find_symbol(*strs.rbegin());
                            return e->inferred_types[sym]();
                        }
                        else
                        {
                            // TODO: No use declaration in this module. This should be
                            // either a warning or an error.
                            return TypeInfo { UnknownType() };
                        }
                    }
                    else
                    {
                        // TODO: Appropriate error condition.
                        return TypeInfo { UnknownType() };
                    }
                },
                engine, n, { module_scope->current_scope }
            };
        return {};
    }

    any InferenceVisitor::visit(RelativeIdentifier* n)
    {
        // We don't want to make a habit of editing the AST in this phase,
        // but relative identifiers are a little special. If we do an earlier
        // declaration pass, we should move this bit of logic into there.
        n->module_name = module_scope->name;

        // Most of the rest is the same as for fully-qualified identifiers,
        // except that we already have the start of the full module name,
        // so we use a slightly different method of creating the whole thing.
        for (auto& c : n->children)
        {
            c->visit(this);
        }

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<RelativeIdentifier*>(node);

                    std::vector<std::string> strs;
                    std::transform(
                        derived->children.begin(),
                        derived->children.end(),
                        std::back_inserter(strs),
                        [](const auto& item) { return item->name; }
                    );

                    // Get the full module name, including the enclosing module
                    // to which this node is relative.
                    auto module_name = fmt::format("{0}:{1}",
                        derived->module_name,
                        fmt::join(strs.begin(), strs.end() - 1, ":")
                    );

                    if (e->module_scopes.count(module_name) > 0)
                    {
                        // This module has been added at some point during the compile,
                        // so now we check to ensure that the current module uses it.
                        auto containing_module = e->module_scopes[module_name].get();

                        auto sym = containing_module->find_symbol(*strs.rbegin());
                        return e->inferred_types[sym]();
                    }
                    else
                    {
                        // TODO: Appropriate error condition.
                        return TypeInfo { UnknownType() };
                    }
                },
                engine, n, { module_scope->current_scope }
            };
        return {};
    }

    any InferenceVisitor::visit(BinaryOp* n)
    {
        n->left->visit(this);
        n->right->visit(this);
        
        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<BinaryOp*>(node);
                    auto lhs = e->inferred_types[derived->left.get()]();
                    auto rhs = e->inferred_types[derived->right.get()]();

                    if (is_boolean_op(derived->op))
                    {
                        return TypeInfo {SimpleType(BasicType::Boolean, false)};
                    }
                    else if (lhs == rhs)
                    {
                        return lhs;
                    }
                    else if (lhs == SimpleType(BasicType::Promoted))
                    {
                        return rhs;
                    }
                    else if (rhs == SimpleType(BasicType::Promoted))
                    {
                        return lhs;
                    }
                    else
                    {
                        return TypeInfo {UnknownType()};
                    }
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(UnaryOp* n)
    {
        n->operand->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<UnaryOp*>(node);

                    auto operand_type = e->inferred_types[derived->operand.get()]();
                    
                    // Different unary operators have different type implications.
                    switch (derived->op)
                    {
                        case UnaryOperators::Coerce:
                            return TypeInfo { SimpleType(BasicType::Promoted) };
                        case UnaryOperators::Dereference:
                        {
                            auto as_ptr_type = util::get_if<PointerType>(&operand_type.type());

                            if (as_ptr_type != nullptr)
                            {
                                // We have a pointer type, so dereference it.
                                return *(as_ptr_type->pointed_type.get());
                            }
                            else
                            {
                                // TODO: Trying to deref a non-pointer. This will be an error
                                // if and only if we decide not to allow overloading the deref
                                // operator. Which is probably what we'll do, unless we want to
                                // allow custom pointers. That...doesn't seem like a good idea.
                                return TypeInfo {UnknownType()};
                            }
                        }
                        case UnaryOperators::Ref:
                        {
                            ReferenceType ref_type;
                            ref_type.referred_type = std::make_shared<TypeInfo>(operand_type);
                            return TypeInfo { ref_type };
                        }
                        case UnaryOperators::Ptr:
                        {
                            PointerType ptr_type;
                            ptr_type.pointed_type = std::make_shared<TypeInfo>(operand_type);
                            return TypeInfo { ptr_type };
                        }
                        default:
                            // All others keep the type of their operand.
                            return operand_type;
                    }
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(TernaryOp* n)
    {
        n->condition->visit(this);
        n->true_branch->visit(this);
        n->false_branch->visit(this);
        
        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<TernaryOp*>(node);
                    auto tb = e->inferred_types[derived->true_branch.get()]();
                    auto fb = e->inferred_types[derived->false_branch.get()]();

                    if (tb == fb)
                    {
                        return tb;
                    }
                    else
                    {
                        return TypeInfo {UnknownType()};
                    }
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(Array* n)
    {
        for (auto& i : n->items)
        {
            i->visit(this);
        }

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Array*>(node);

                    ArrayType ti;
                    ti.size = derived->items.size();

                    if (derived->items.empty())
                    {
                        // A list expression shouldn't be empty, but just in case,
                        // we treat it as a list of nothing.
                        ti.base_type = std::make_shared<TypeInfo>(NothingType());
                    }
                    else
                    {
                        // All entries in a list expression must be of compatible
                        // types. We use the first as a comparison. If everything
                        // else matches it, good. If not, there might be conversion
                        // functions defined somewhere, so let later phases figure
                        // that out.
                        auto first = e->inferred_types[derived->items[0].get()]();

                        ti.base_type = std::make_shared<TypeInfo>(first);
                    }

                    return ti;
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(List* n)
    {
        for (auto& i : n->items)
        {
            i->visit(this);
        }

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<List*>(node);

                    ListType ti;

                    if (derived->items.empty())
                    {
                        // A list expression shouldn't be empty, but just in case,
                        // we treat it as a list of nothing.
                        ti.base_type = std::make_shared<TypeInfo>(NothingType());
                    }
                    else
                    {
                        // All entries in a list expression must be of compatible
                        // types. We use the first as a comparison. If everything
                        // else matches it, good. If not, there might be conversion
                        // functions defined somewhere, so let later phases figure
                        // that out.
                        auto first = e->inferred_types[derived->items[0].get()]();

                        ti.base_type = std::make_shared<TypeInfo>(first);
                    }

                    return ti;
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(Tuple* n)
    {
        for (auto& i : n->items)
        {
            i->visit(this);
        }

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Tuple*>(node);

                    TupleType ti;

                    for (auto& i : derived->items)
                    {
                        ti.contained_types.emplace_back(
                            std::make_shared<TypeInfo>(e->inferred_types[i.get()]())
                        );
                    }

                    return ti;
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(DictionaryEntry* n)
    {
        n->value->visit(this);

        util::visit([&](auto& el) { el->visit(this); }, n->key);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<DictionaryEntry*>(node);
                    return e->inferred_types[derived->value.get()]();
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(Dictionary* n)
    {
        for (auto&& item : n->items)
        {
            item->visit(this);
        }

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Dictionary*>(node);

                    DictionaryType dt;
                    if (derived->items.empty())
                    {
                        dt.base_type = std::make_shared<TypeInfo>(NothingType());
                    }
                    else
                    {
                        auto first = e->inferred_types[derived->items[0]->value.get()]();
                        if (
                            std::all_of(
                                derived->items.begin()+1,
                                derived->items.end(),
                                [&](const auto& el)
                                {
                                    return compatible(first, e->inferred_types[el->value.get()]());
                                }
                            )
                        )
                        {
                            dt.base_type = std::make_shared<TypeInfo>(first);
                        }
                        else
                        {
                            // The entries have different types, which should be an error
                            // if they're not compatible.
                            dt.base_type = std::make_shared<TypeInfo>(NothingType());
                        }
                    }

                    return TypeInfo { dt };
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(Subscript* n)
    {
        n->index->visit(this);
        n->container->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Subscript*>(node);

                    auto target_type = e->inferred_types[derived->container.get()]();
                    auto as_array_type = util::get_if<ArrayType>(&target_type.type());
                    if (as_array_type != nullptr)
                    {
                        // If we're taking the subscript of an array type, we already know
                        // how to do that.
                        return *(as_array_type->base_type);
                    }
                    else
                    {
                        // TODO: Handle any other possibilities.
                        return TypeInfo { UnknownType() };
                    }
                },
                engine, n, { module_scope->current_scope }
            };

        return {};
    }

    any InferenceVisitor::visit(Member* n)
    {
        n->object->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Member*>(node);

                    auto object_type = e->inferred_types[derived->object.get()]();
                    auto as_struct_type = util::get_if<StructureType>(&object_type.type());
                    if (as_struct_type != nullptr)
                    {
                        auto result = std::find_if(
                            as_struct_type->fields.begin(),
                            as_struct_type->fields.end(),
                            [&](const auto& e)
                            {
                                return e.first == derived->member->name;
                            }
                        );

                        if (result != as_struct_type->fields.end())
                        {
                            return *(result->second);
                        }
                        else
                        {
                            // TODO: Field not found. This should be an error, but here?
                            return TypeInfo { UnknownType() };
                        }
                    }
                    else
                    {
                        // TODO Handle other possibilities.
                        return TypeInfo { UnknownType() };
                    }
                },
                engine, n, { module_scope->current_scope }
            };

        return {};
    }

    any InferenceVisitor::visit(BuiltinType* n)
    {
        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<BuiltinType*>(node);

                    return TypeInfo { SimpleType(derived->type) };
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(SimpleTypename* n)
    {
        n->name->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<SimpleTypename*>(node);

                    auto mapped = e->inferred_types[derived->name.get()]();
                    // Any other processing to do here?
                    return mapped;
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(GenericTypename* n)
    {
        n->name->visit(this);
        for (auto& c : n->children)
        {
            c->visit(this);
        }

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<GenericTypename*>(node);

                    GenericType g;

                    g.base_type = std::make_shared<TypeInfo>(
                        e->mapper.get_type_for(derived->name->canonical_name()
                    ));

                    for (auto& gp : derived->children)
                    {
                        g.concrete_types.emplace_back(
                            std::move(std::make_shared<TypeInfo>(
                                e->inferred_types[gp.get()]()
                            ))
                        );
                    }

                    return TypeInfo { g };
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(VectorTypename* n)
    {
        n->type->visit(this);
        n->size->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<VectorTypename*>(node);

                    ArrayType a {
                        std::make_shared<TypeInfo>(e->inferred_types[derived->type.get()]()),
                        0u
                    };

                    return TypeInfo { a };
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(Variant* n)
    {
        for (auto&& ch : n->children)
        {
            ch->visit(this);
        }
        
        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Variant*>(node);

                    VariantType tinfo;
                    for (auto& ch : derived->children)
                    {
                        auto ct = e->inferred_types[ch.get()]();
                        tinfo.types.emplace_back(std::move(std::make_unique<TypeInfo>(ct)));
                    }

                    // return e->mapper.get_type_for(derived->canonical_name());
                    return TypeInfo { tinfo };
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(Optional* n)
    {
        n->type->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Optional*>(node);

                    auto t = e->inferred_types[derived->type.get()]();
                    OptionalType tinfo;
                    tinfo.contained_type = std::make_shared<TypeInfo>(t);

                    return TypeInfo { tinfo };
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(Cast* n)
    {
        n->left->visit(this);
        n->right->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Cast*>(node);

                    return e->mapper.get_type_for(derived->right->canonical_name());
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(TypeCheck* n)
    {
        n->left->visit(this);
        n->right->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    return SimpleType(BasicType::Boolean);
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(Alias* n)
    {
        // Aliases only affect the symbol table, and they are not hoisted; an alias
        // only applies to code in the current scope (and its children) following
        // the point of declaration.
        module_scope->add_symbol(n->alias->canonical_name(), n);

        n->original->visit(this);

        engine->mapper.add_type_definition(
            n->alias->canonical_name(),
            engine->inferred_types[n->original.get()]()
        );

        return {};
    }

    any InferenceVisitor::visit(Enum* n)
    {
        n->name->visit(this);
        n->values->visit(this);

        EnumType tinfo;
        tinfo.name = n->name->canonical_name();

        std::transform(
            n->values->symbols.begin(),
            n->values->symbols.end(),
            std::back_inserter(tinfo.values),
            [&](const auto& el) { return el->value; }
        );
        
        engine->mapper.add_type_definition(
            n->name->canonical_name(),
            tinfo
        );

        return {};
    }

    any InferenceVisitor::visit(SymbolList* n)
    {
        for (auto&& ch : n->symbols)
        {
            ch->visit(this);
        }

        return {};
    }

    // Most statements don't have types themselves, but they'll still need to descend
    // into their child nodes for the expressions.
    any InferenceVisitor::visit(If* n)
    {
        n->condition->visit(this);
        n->then_case->visit(this);
        if (n->else_case != nullptr)
        {
            n->else_case->visit(this);
        }

        return {};
    }

    any InferenceVisitor::visit(BareExpression* n)
    {
        n->expression->visit(this);

        return {};
    }

    any InferenceVisitor::visit(Assign* n)
    {
        // We visit the RHS first because it needs to be fully evaluated before
        // it can be assigned.
        n->rhs->visit(this);
        n->lhs->visit(this);

        return {};
    }

    any InferenceVisitor::visit(CompoundAssign* n)
    {
        // See above for reasoning on the order.
        n->rhs->visit(this);
        n->lhs->visit(this);

        return {};
    }

    any InferenceVisitor::visit(Block* n)
    {
        // Blocks always create a new scope
        module_scope->begin_scope("$block");


        for (auto&& ch : n->children)
        {
            ch->visit(this);
        }

        module_scope->end_scope();

        return {};
    }

    any InferenceVisitor::visit(While* n)
    {
        n->condition->visit(this);
        n->body->visit(this);
        return {};
    }

    any InferenceVisitor::visit(For* n)
    {
        // For loops introduce a loop variable, so we have to account for that.
        // As it's stored as a string rather than a node pointer, we'll use this
        // node's pointer as the index into the map. Also, this implicitly creates
        // a new scope, as the loop variable cannot be referenced outside the loop.
        module_scope->begin_scope("$for");

        module_scope->add_symbol(n->index, n);

        n->range->visit(this);
        n->body->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<For*>(node);

                    // For loops have no actual types, because Rhea considers
                    // them statements instead of expressions. But we're using
                    // a pointer to this node as a symbol table entry for our
                    // index variable, which *does* need inference.
                    //
                    // As we're iterating over a range (usually, bot not always,
                    // an array), it makes sense to use some of the subscript logic.
                    auto target_type = e->inferred_types[derived->range.get()]();
                    auto as_array_type = util::get_if<ArrayType>(&target_type.type());
                    if (as_array_type != nullptr)
                    {
                        // If we're taking the subscript of an array type, we already know
                        // how to do that.
                        return *(as_array_type->base_type);
                    }
                    else
                    {
                        // TODO: Handle any other possibilities.
                        return TypeInfo { UnknownType() };
                    }

                    return TypeInfo { UnknownType() };
                },
                engine, n
            };

        module_scope->end_scope();

        return {};
    }

    any InferenceVisitor::visit(With* n)
    {
        n->body->visit(this);

        for (auto&& ch : n->predicates)
        {
            ch->visit(this);
        }

        return {};
    }

    any InferenceVisitor::visit(Break* n)
    {
        // Breaks need no type inference
        return {};
    }

    any InferenceVisitor::visit(Continue* n)
    {
        // Continues need no type inference
        return {};
    }

    any InferenceVisitor::visit(Match* n)
    {
        n->expression->visit(this);

        for (auto&& ch : n->cases)
        {
            ch->visit(this);
        }

        return {};
    }

    any InferenceVisitor::visit(On* n)
    {
        n->case_expr->visit(this);
        n->body->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<On*>(node);

                    return e->inferred_types[derived->case_expr.get()]();
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(When* n)
    {
        n->predicate->visit(this);
        n->body->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    // Predicates always return a boolean.
                    return SimpleType(BasicType::Boolean, false);
                },
                engine, n
            };
        
        return {};
    }

    any InferenceVisitor::visit(TypeCase* n)
    {
        n->type_name->visit(this);
        n->body->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<TypeCase*>(node);

                    return e->mapper.get_type_for(derived->type_name->canonical_name());
                },
                engine, n, { module_scope->current_scope }
            };
            
        return {};
    }

    any InferenceVisitor::visit(Default* n)
    {
        // Defaults need no type inference
        return {};
    }

    any InferenceVisitor::visit(PredicateCall* n)
    {
        n->target->visit(this);

        for (auto&& ch : n->arguments)
        {
            ch->visit(this);
        }

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    return SimpleType(BasicType::Boolean);
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(TypeDeclaration* n)
    {
        n->lhs->visit(this);
        n->rhs->visit(this);

        module_scope->add_symbol(n->lhs->canonical_name(), n);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<TypeDeclaration*>(node);

                    return e->inferred_types[derived->rhs.get()]();
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(Variable* n)
    {
        n->lhs->visit(this);
        n->rhs->visit(this);

        module_scope->add_symbol(n->lhs->canonical_name(), n);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Variable*>(node);

                    if (e->inferred_types.count(derived->rhs.get()) != 0)
                    {
                        return e->inferred_types[derived->rhs.get()]();
                    }
                    else
                    {
                        return TypeInfo {UnknownType()};
                    }
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(Constant* n)
    {
        n->lhs->visit(this);
        n->rhs->visit(this);

        module_scope->add_symbol(n->lhs->canonical_name(), n);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Constant*>(node);

                    if (e->inferred_types.count(derived->rhs.get()) != 0)
                    {
                        return e->inferred_types[derived->rhs.get()]();
                    }
                    else
                    {
                        return TypeInfo {UnknownType()};
                    }
                },
                engine, n
            };
            
        return {};
    }

    any InferenceVisitor::visit(NamedArgument* n)
    {
        n->value->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<NamedArgument*>(node);

                    return e->inferred_types[derived->value.get()]();
                },
                engine, n
            };
        return {};
    }

    any InferenceVisitor::visit(Call* n)
    {
        // We have to figure out which function is being called, as overloads
        // and module imports can bring a large number of functions together
        // under one name.

        // TODO: This is a very big, very complex algorithm, one that probably
        // needs to be split off into its own source file for ease of understanding.
        // At the moment, we're just going to look for a single exact match
        // somewhere in the current scope's tree.

        // First, visit each argument. And the "target", too, because it might
        // be a method-style call.
        for (auto& a : n->arguments)
        {
            // Arguments are variants, because they might be named or positional.
            util::visit([&](const auto& e) { return e->visit(this); }, a);
        }
        n->target->visit(this);
        
        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Call*>(node);

                    // We have to find the name of the function being called.
                    // That's harder than it looks, because we might be dealing
                    // with a member expression. And the Expression AST node class
                    // doesn't have a handy "name" method.
                    // TODO: Do we need to handle anything besides identifiers
                    // and members?
                    std::string function_name;

                    {
                        auto expr = derived->target.get();
                        auto as_identifier = dynamic_cast<AnyIdentifier*>(expr);
                        if (as_identifier != nullptr)
                        {
                            // Target is an identifier, so we can get the name from that.
                            // TODO: Handle fully-qualified or relative identifiers.
                            function_name = as_identifier->canonical_name();
                        }
                        else
                        {
                            // This must be a method-style call.
                            auto as_member = dynamic_cast<Member*>(expr);
                            if (as_member != nullptr)
                            {
                                function_name = as_member->member->name;
                            }
                            else
                            {
                                // Or maybe it isn't. This may happen if we change parsing.
                                // TODO: Error or whatever.
                            }
                        }
                    }

                    // Now that we have the name, let's look for a function with that name.
                    {
                        auto scope { state.scope };

                        while (scope != nullptr)
                        {
                            auto defs_in_scope = scope->find_function_definitions(function_name);

                            if (defs_in_scope.first != defs_in_scope.second)
                            {
                                // Each result is a (name, node) pair.
                                auto def_node = defs_in_scope.first->second;
                                auto ftype = e->inferred_types[def_node]();

                                auto as_function = util::get_if<FunctionType>(&ftype.type());
                                assert(as_function != nullptr);

                                return *(as_function->return_type);
                            }

                            scope = scope->parent;
                        }
                    }

                    return TypeInfo { UnknownType() };
                },
                engine, n, { module_scope->current_scope }
            };
        return {};
    }

    any InferenceVisitor::visit(Def* n)
    {
        // We need a way to handle overloaded functions in the same scope.
        // As a quick and dirty method, we alter their symbols using what
        // is basically an "unprintable" identifier.
        //
        // Note that we can't just use the mangled name here, because it
        // may rely on types that haven't been defined yet.
        // module_scope->add_symbol(n->function_type_string(), n);
        module_scope->add_symbol(n->name + "@" + std::to_string(reinterpret_cast<std::uintptr_t>(n)), n);

        // Adding to the function definition table is much easier, because
        // it's a multimap, so it's designed to handle overloads.
        module_scope->add_function_definition(n->name, n);

        module_scope->begin_scope(n->name);

        // We also create a new return list. Since we'll be traversing the
        // function's body, we can look for return statements here and store
        // them for later.
        ReturnList rl { n, {} };
        function_definition_returns.push(rl);

        if (n->return_type != nullptr) n->return_type->visit(this);
        if (n->arguments_list != nullptr) n->arguments_list->visit(this);

        for (auto&& con : n->conditions)
        {
            con->visit(this);
        }
        n->body->visit(this);

        /*
         * Laziness comes back to bite us here, unfortunately. Since we can't
         * necessarily know all the needed types to make an inference record
         * for the function right now, we have to call it unknown, then determine
         * it later on, once we've gone over the full program or module.
         */
        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Def*>(node);

                    types::FunctionType ft;
                    ft.definition_type = derived->type;

                    if (derived->return_type != nullptr)
                    {
                        ft.return_type = std::make_shared<TypeInfo>(
                            e->inferred_types[derived->return_type.get()]()
                        );
                    }
                    else
                    {
                        assert(state.return_list.function == derived);

                        auto rl = state.return_list.returns;
                        if (rl.empty())
                        {
                            // A function with no return statements implicitly
                            // returns nothing.
                            ft.return_type = std::make_shared<TypeInfo>(NothingType());
                        }
                        else if (rl.size() == 1)
                        {
                            // A function with 1 return statement returns the type
                            // of that statement's expression.
                            ft.return_type = std::make_shared<TypeInfo>(e->inferred_types[rl[0]]());
                        }
                        else
                        {
                            // Multiple return statements. These must be compatible.
                            auto first_return = e->inferred_types[rl[0]]();
                            auto comp = std::all_of(
                                rl.begin()+1,
                                rl.end(),
                                [&](const auto& el) { return compatible(e->inferred_types[el](), first_return); }
                            );

                            if (comp)
                            {
                                ft.return_type = std::make_shared<TypeInfo>(first_return);
                            }
                            else
                            {
                                ft.return_type = std::make_shared<TypeInfo>(UnknownType());
                            }
                        }
                    }

                    if (derived->arguments_list != nullptr)
                    {
                        if (derived->arguments_list->arguments.size() == 1 &&
                            derived->arguments_list->arguments[0]->value->canonical_name() == "$$wildcard$$")
                        {
                            // Special case for the wildcard argument
                            ft.argument_types.push_back(std::make_pair(
                                derived->arguments_list->arguments[0]->name,
                                std::make_shared<TypeInfo>(TupleType())
                            ));
                        }
                        else
                        {
                            for (auto&& a : derived->arguments_list->arguments)
                            {
                                ft.argument_types.push_back(std::make_pair(
                                    a->name,
                                    std::make_shared<TypeInfo>(e->inferred_types[a.get()]())
                                ));
                            }
                        }
                    }

                    return ft;
                },
                engine, n, { module_scope->current_scope, function_definition_returns.top() }
            };

        function_definition_returns.pop();
        module_scope->end_scope();
        return {};
    }

    any InferenceVisitor::visit(GenericDef* n)
    {
        // Generic functions are a little tricky. For the most part, we can
        // treat them like regular definitions, but we also need to record and track
        // the generic parts.

        for (auto& g : n->generic_types)
        {
            // Generic types are variants, so do it this way.
            util::visit([&](auto& e) { e->visit(this); }, g);
        }

        // Now, we do almost all the same steps as for concrete definitions,
        // but the evalutation has to change.

        module_scope->add_symbol(n->name + "@" + std::to_string(reinterpret_cast<std::uintptr_t>(n)), n);
        module_scope->add_function_definition(n->name, n);
        module_scope->begin_scope(n->name);

        ReturnList rl { n, {} };
        function_definition_returns.push(rl);

        if (n->return_type != nullptr) n->return_type->visit(this);
        if (n->arguments_list != nullptr) n->arguments_list->visit(this);

        for (auto&& con : n->conditions)
        {
            con->visit(this);
        }
        n->body->visit(this);

        // See the Def version above for reasoning on this.
        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<GenericDef*>(node);

                    types::FunctionType ft;
                    ft.definition_type = derived->type;

                    if (derived->return_type != nullptr)
                    {
                        ft.return_type = std::make_shared<TypeInfo>(
                            e->inferred_types[derived->return_type.get()]()
                        );
                    }
                    else
                    {
                        assert(state.return_list.function == derived);

                        auto rl = state.return_list.returns;
                        if (rl.empty())
                        {
                            // A function with no return statements implicitly
                            // returns nothing.
                            ft.return_type = std::make_shared<TypeInfo>(NothingType());
                        }
                        else if (rl.size() == 1)
                        {
                            // A function with 1 return statement returns the type
                            // of that statement's expression.
                            ft.return_type = std::make_shared<TypeInfo>(e->inferred_types[rl[0]]());
                        }
                        else
                        {
                            // Multiple return statements. These must be compatible.
                            auto first_return = e->inferred_types[rl[0]]();
                            auto comp = std::all_of(
                                rl.begin()+1,
                                rl.end(),
                                [&](const auto& el) { return compatible(e->inferred_types[el](), first_return); }
                            );

                            if (comp)
                            {
                                ft.return_type = std::make_shared<TypeInfo>(first_return);
                            }
                            else
                            {
                                ft.return_type = std::make_shared<TypeInfo>(UnknownType());
                            }
                        }
                    }

                    if (derived->arguments_list != nullptr)
                    {
                        if (derived->arguments_list->arguments.size() == 1 &&
                            derived->arguments_list->arguments[0]->value->canonical_name() == "$$wildcard$$")
                        {
                            // Special case for the wildcard argument
                            ft.argument_types.push_back(std::make_pair(
                                derived->arguments_list->arguments[0]->name,
                                std::make_shared<TypeInfo>(TupleType())
                            ));
                        }
                        else
                        {
                            for (auto&& a : derived->arguments_list->arguments)
                            {
                                ft.argument_types.push_back(std::make_pair(
                                    a->name,
                                    std::make_shared<TypeInfo>(e->inferred_types[a.get()]())
                                ));
                            }
                        }
                    }

                    // Here's the difference for generic defs: we add in the generic
                    // typenames and wrap into another object.
                    GenericFunctionType gft { ft };
                    for (auto& g : derived->generic_types)
                    {
                        auto name = util::visit([](const auto& e) { return e->name; }, g);
                        auto p = util::visit([](const auto& e) { return static_cast<ASTNode*>(e.get()); }, g);
                        auto ti = e->inferred_types[p]();
                        gft.generic_matches.push_back({ name, std::make_shared<TypeInfo>(ti) });
                    }

                    return gft;
                },
                engine, n, { module_scope->current_scope, function_definition_returns.top() }
            };

        function_definition_returns.pop();
        module_scope->end_scope();
        return {};
    }

    any InferenceVisitor::visit(Structure* n)
    {
        module_scope->add_symbol(n->name->canonical_name(), n);

        module_scope->begin_scope("$structdef$" + n->name->canonical_name());

        for (auto& f : n->fields)
        {
            f->visit(this);
        }

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Structure*>(node);

                    StructureType ti;
                    ti.name = derived->name->canonical_name();
                    for (auto& f : derived->fields)
                    {
                        ti.fields.emplace_back(
                            std::make_pair(
                                f->name,
                                std::make_shared<TypeInfo>(e->inferred_types[f->value.get()]())
                            )
                        );
                    }

                    TypeInfo stinfo { ti };
                    auto cname = canonical_name(stinfo);

                    if (!e->mapper.is_type_defined(cname))
                    {
                        e->mapper.add_type_definition(cname, stinfo);
                    }

                    return stinfo;
                },
                engine, n, { module_scope->current_scope }
            };

        module_scope->end_scope();

        return {};
    }

    any InferenceVisitor::visit(Arguments* n)
    {
        for (auto&& a : n->arguments)
        {
            a->visit(this);
        }

        return {};
    }

    any InferenceVisitor::visit(TypePair* n)
    {
        // Use this node as the reference, and we add it to the local symbol table.
        module_scope->add_symbol(n->name, n);

        // The name is just a string, so we don't have to visit it, but the value...
        n->value->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<TypePair*>(node);

                    // return e->mapper.get_type_for(derived->value->canonical_name());
                    return e->inferred_types[derived->value.get()]();
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(Return* n)
    {
        // Returns need to visit the expression. We'll also store that inferred type
        // with this node. That should help us with function type inference.
        n->value->visit(this);

        // We add to the current function's return list. Returns are always scoped
        // to the nearest function. (Rhea doesn't yet handle nested defs, but this
        // will make it easier if we ever do.)
        if (!function_definition_returns.empty())
        {
            function_definition_returns.top().returns.push_back(n);
        }
        // TODO: Returns outside a function should be an error, right?

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Return*>(node);

                    return e->inferred_types[derived->value.get()]();
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(Try* n)
    {
        // Try block has no type information, so we just have to visit its children.
        n->body->visit(this);
        for (auto& c : n->catches)
        {
            c->visit(this);
        }
        if (n->finally_block != nullptr)
        {
            n->finally_block->visit(this);
        }

        return {};
    }

    any InferenceVisitor::visit(Catch* n)
    {
        // For a catch, we visit the children, and we store the type of the caught
        // exception as that of the whole statement.

        // The capture variable is in its own scope.
        module_scope->begin_scope("$catch");

        n->catch_type->visit(this);
        n->body->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Catch*>(node);

                    return e->inferred_types[derived->catch_type.get()]();
                },
                engine, n
            };

        module_scope->end_scope();

        return {};
    }

    any InferenceVisitor::visit(Throw* n)
    {
        // A throw statement just holds the type information for the expression
        // being thrown.
        n->exception->visit(this);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Throw*>(node);

                    return e->inferred_types[derived->exception.get()]();
                },
                engine, n
            };

        return {};
    }

    any InferenceVisitor::visit(Finally* n)
    {
        // Finally block has no type information.
        n->body->visit(this);

        return {};
    }

    any InferenceVisitor::visit(Extern* n)
    {
        // Externs need to run the type inference for their argument types and returns.
        // They have no code of their own, though.
        for (auto& a : n->argument_types)
        {
            a->visit(this);
        }
        if (n->return_type != nullptr) n->return_type->visit(this);

        // Externs can't be overloaded, so we give them a fixed name in
        // the module symbol table.
        module_scope->add_symbol(n->name + "@extern", n);

        engine->inferred_types[n] =
            InferredType {
                [](TypeEngine* e, ASTNode* node, InferenceState state)
                {
                    auto derived = static_cast<Extern*>(node);

                    types::FunctionType ft;
                    ft.definition_type = FunctionClass::Unchecked;

                    if (derived->return_type != nullptr)
                    {
                        ft.return_type =
                            std::make_shared<TypeInfo>(e->inferred_types[derived->return_type.get()]());
                    }
                    else
                    {
                        ft.return_type = std::make_shared<TypeInfo>(NothingType());
                    }

                    for (auto& a : derived->argument_types)
                    {
                        // Externs don't allow named arguments, because we're not in control
                        // of their codegen. But type info objects for functions require them.
                        ft.argument_types.emplace_back(
                            "unnamed",
                            std::make_shared<TypeInfo>(e->inferred_types[a.get()]())
                        );
                    }

                    return TypeInfo { ft };
                },
                engine, n, { module_scope->current_scope }
            };

        return {};
    }

    any InferenceVisitor::visit(Concept* n)
    {
        // Concepts keep a vector of child nodes, but these are themselves variants.
        for (auto& b : n->body)
        {
            util::visit([&](const auto& e) { e->visit(this); }, b);
        }

        // We do define a new type, if an abstract one.
        engine->mapper.add_type_definition(
            n->name,
            ConceptType { n-> name }
        );

        return {};
    }

    any InferenceVisitor::visit(ConceptMatch* n)
    {
        // This is really the same as a named argument, just with a concept instead
        // of a concrete type. Not much to do here.

        n->concept_type->visit(this);

        return {};
    }

    any InferenceVisitor::visit(FunctionCheck* n)
    {
        // Just visit the child nodes to get their type info loaded.

        n->function_name->visit(this);
        n->return_type_name->visit(this);
        for (auto& a : n->function_arguments)
        {
            a->visit(this);
        }

        return {};
    }

    any InferenceVisitor::visit(ModuleDef* n)
    {
        if (engine->module_scopes.count(n->name->name))
        {
            // This module has already been defined.
            // TODO: This should be an error.
        }
        else
        {
            // Defining a new module.
            engine->module_scopes[n->name->name] = std::make_unique<state::ModuleScopeTree>(n->name->name);
            module_scope = engine->module_scopes[n->name->name].get();
        }

        return {};
    }

    any InferenceVisitor::visit(Import* n)
    {
        // Imports don't really affect type inference, as they're just bringing
        // symbols into scope, but we can do that here if it hasn't already been
        // done.

        // TOOD: This should load and (if necessary) compile the module, unless
        // an earlier pass has done it already.

        if (engine->module_scopes.count(n->module->name) == 0)
        {
            // Module hasn't been loaded yet.
            // TODO: Process.
            engine->module_scopes[n->module->name] = std::make_unique<state::ModuleScopeTree>(n->module->name);
        }

        auto imported_module = engine->module_scopes[n->module->name].get();
        module_scope->imports.push_back(imported_module);

        std::vector<std::string> all_symbol_names;
        std::transform(
            std::begin(imported_module->root->symbol_table),
            std::end(imported_module->root->symbol_table),
            std::back_inserter(all_symbol_names),
            [](const auto& item) { return item.first; }
        );

        for (auto& im : n->imports)
        {
            auto pos = std::find(
                std::begin(imported_module->exports),
                std::end(imported_module->exports),
                im->canonical_name()
            );

            if (pos != std::end(imported_module->exports))
            {
                // This identifier has been exported by the module.

                auto sym = imported_module->find_symbol(im->canonical_name());

                if (sym != nullptr)
                {
                    // The identifier itself was found in the module's symbol table.
                    // This will be the case for constants and typenames.

                    // Copy the inference node into the current module, but don't
                    // evaluate it.
                    sym->visit(this);
                }
                else
                {
                    // This identifier wasn't found as a symbol, but it was exported
                    // by the module. Simple deduction tells us that it must be
                    // a function, possibly with overloads, so we have to find them
                    // and copy all of them.

                    // At the moment, we only check the "root" scope. That is, the
                    // top level of the module. That's a fair assumption, since
                    // imports must be defined at the module level.
                    std::for_each(
                        std::begin(imported_module->root->symbol_table),
                        std::end(imported_module->root->symbol_table),
                        [&](const auto& item)
                        {
                            if (boost::algorithm::starts_with(item.first, im->canonical_name()))
                            {
                                module_scope->add_symbol(item.first, item.second);
                                item.second->visit(this);
                            }
                        }
                    );
                }
            }
            else
            {
                // TODO: Some kind of invalid import error
            }
        }

        return {};
    }
    
    any InferenceVisitor::visit(Export* n)
    {
        // Exporting needs no type inference, but it does need to add
        // the given identifiers to the exports list for this module.

        // TODO: Consider putting this and other things into an earlier
        // declaration pass.
        for (auto& e : n->exports)
        {
            module_scope->exports.push_back(e->canonical_name());
        }

        return {};
    }

    any InferenceVisitor::visit(Use* n)
    {
        // Use declarations don't actually do anything type-wise.
        // Instead, here we'll check to make sure there's a scope tree
        // defined for the module.

        // TODO: We need to load and (maybe) compile the module at some point,
        // either here or in an earlier pass.
        if (engine->module_scopes.count(n->module->name) == 0)
        {
            // This module hasn't been loaded yet.
            engine->module_scopes[n->module->name] =
                std::make_unique<state::ModuleScopeTree>(n->module->name);
        }

        module_scope->imports.push_back(engine->module_scopes[n->module->name].get());

        return {};
    }

}}
