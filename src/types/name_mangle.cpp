#include "types/name_mangle.hpp"

namespace rhea { namespace types {
    using ast::unimplemented_type;
    using ast::FunctionClass;

    namespace internal
    {
        std::string name_mangle_visitor::operator()(const UnknownType& t) const
        {
            throw rhea::ast::unimplemented_type(to_string(t));
        }

        std::string name_mangle_visitor::operator()(const SimpleType& t) const
        {
            switch (t.type)
            {
                case BasicType::Integer:
                    return "i";
                case BasicType::Byte:
                    return "c";
                case BasicType::Long:
                    return "l";
                case BasicType::UnsignedInteger:
                    return "I";
                case BasicType::UnsignedByte:
                    return "C";
                case BasicType::UnsignedLong:
                    return "L";
                case BasicType::Float:
                    return "Df";
                case BasicType::Double:
                    return "Dd";
                case BasicType::Boolean:
                    return "b";
                case BasicType::Symbol:
                    return "Sy";
                case BasicType::String:
                    return "s";
                // These are repeats for when we get an any/nothing *basic* type object.
                case BasicType::Any:
                    return "a";
                case BasicType::Nothing:
                    return "v";
                default:
                    throw ast::unimplemented_type(to_string(t));
            }
        }

        std::string name_mangle_visitor::operator()(const NothingType& t) const
        {
            return "v";
        }

        std::string name_mangle_visitor::operator()(const ReferenceType& t) const
        {
            return "r"s + mangle_name(*(t.referred_type));
        }

        std::string name_mangle_visitor::operator()(const PointerType& t) const
        {
            return "p"s + mangle_name(*(t.pointed_type));
        }

        std::string name_mangle_visitor::operator()(const OptionalType& t) const
        {
            return "Op"s + mangle_name(*(t.contained_type));
        }

        std::string name_mangle_visitor::operator()(const VariantType& t) const
        {
            auto result = "V"s + std::to_string(t.types.size()) + "_"s;
            for (auto&& vt : t.types)
            {
                result += mangle_name(*vt);
            }
            return result;
        }

        std::string name_mangle_visitor::operator()(const ArrayType& t) const
        {
            return fmt::format("A{0}_{1}", t.size, mangle_name(*(t.base_type)));
        }

        std::string name_mangle_visitor::operator()(const GenericType& t) const
        {
            std::vector<std::string> strs;
            std::transform(
                t.concrete_types.begin(),
                t.concrete_types.end(),
                std::back_inserter(strs),
                [](const auto& e) { return mangle_name(*e); }
            );

            return fmt::format("G{0}{1}_{2}",
                mangle_name(*(t.base_type)),
                t.concrete_types.size(),
                fmt::join(strs.begin(), strs.end(), "")
            );
        }

        std::string name_mangle_visitor::operator()(const EnumType& t) const
        {
            return fmt::format("E{0}{1}", t.name.length(), t.name);
        }

        std::string name_mangle_visitor::operator()(const StructureType& t) const
        {
            return fmt::format("Ss{0}{1}", t.name.length(), t.name);
        }

        std::string name_mangle_visitor::operator()(const ListType& t) const
        {
            return "Sl"s + mangle_name(*(t.base_type));
        }

        std::string name_mangle_visitor::operator()(const DictionaryType& t) const
        {
            return "Sd"s + mangle_name(*(t.base_type));
        }

        std::string name_mangle_visitor::operator()(const TupleType& t) const
        {
            std::vector<std::string> strs;
            std::transform(
                t.contained_types.begin(),
                t.contained_types.end(),
                std::back_inserter(strs),
                [](const auto& e) { return mangle_name(*e); }
            );
            return fmt::format("T{0}_{1}",
                t.contained_types.size(),
                fmt::join(strs.begin(), strs.end(), "")
            );
        }

        std::string name_mangle_visitor::operator()(const AnyType& t) const
        {
            return "a";
        }
    }

    std::string mangle_function_name(std::string name, FunctionType function_type,
     FunctionClass function_class)
    {
        // Unchecked functions are unmangled functions
        if (function_class == FunctionClass::Unchecked)
        {
            return name;
        }

        std::string mangled = "_R";

        switch (function_class)
        {
            case FunctionClass::Basic:
                mangled += "f";
                break;
            case FunctionClass::Predicate:
                mangled += "p";
                break;
            case FunctionClass::Operator:
                mangled += "o";
                break;
            default:
                throw unimplemented_type(name);
        }

        if (function_class == FunctionClass::Operator)
        {
            mangled += name;
        }
        else
        {
            mangled += fmt::format("{0}{1}", name.length(), name);
        }

        if (function_type.return_type != nullptr)
        {
            mangled += mangle_name(*(function_type.return_type));
        }
        else
        {
            // A function without a return type implictly returns nothing.
            mangled += "v";
        }

        if (function_type.argument_types.empty())
        {
            mangled += "0";
        }
        else
        {
            for (auto&& t : function_type.argument_types)
            {
                // auto fn = [&] (auto const& e) { return mangle_name(e); };
                // auto result = util::visit(internal::name_mangle_visitor(), t.second->type());
                mangled += mangle_name(*(t.second));
            }
        }

        return mangled;
    }
}}