#include "types/canonical_name.hpp"

namespace rhea { namespace types {
    namespace internal {
        std::string canonical_name_visitor::operator()(const UnknownType& t) const
        {
            return "Unknown";
        }

        std::string canonical_name_visitor::operator()(const SimpleType& t) const
        {
            return basic_type_to_string.at(t.type);
        }

        std::string canonical_name_visitor::operator()(const NothingType& t) const
        {
            return "nothing";
        }

        std::string canonical_name_visitor::operator()(const FunctionType& t) const
        {
            std::vector<std::string> strs;

            std::transform(t.argument_types.begin(), t.argument_types.end(), std::back_inserter(strs),
                [](const auto& e) { return canonical_name(*(e.second)); }
            );

            return fmt::format("<{0}> -> {1}",
                fmt::join(strs.begin(), strs.end(), ","),
                canonical_name(*(t.return_type))
            );
        }

        std::string canonical_name_visitor::operator()(const ReferenceType& t) const
        {
            return fmt::format("Reference({0})", canonical_name(*(t.referred_type)));
        }

        std::string canonical_name_visitor::operator()(const PointerType& t) const
        {
            return fmt::format("Pointer({0})", canonical_name(*(t.pointed_type)));
        }

        std::string canonical_name_visitor::operator()(const OptionalType& t) const
        {
            return fmt::format("Optional({0})", canonical_name(*(t.contained_type)));
        }

        std::string canonical_name_visitor::operator()(const VariantType& t) const
        {
            std::vector<std::string> strs;

            std::transform(t.types.begin(), t.types.end(), std::back_inserter(strs),
                [](const auto& e) { return canonical_name(*e); }
            );

            return fmt::format("Variant({0})",
                fmt::join(strs.begin(), strs.end(), ",")
            );
        }

        std::string canonical_name_visitor::operator()(const ArrayType& t) const
        {
            return fmt::format("{0}[{1}]", canonical_name(*(t.base_type)), t.size);
        }

        std::string canonical_name_visitor::operator()(const GenericType& t) const
        {
            std::vector<std::string> strs;

            std::transform(t.concrete_types.begin(), t.concrete_types.end(), std::back_inserter(strs),
                [](const auto& e) { return canonical_name(*e); }
            );
            
            return fmt::format("{0}<{1}>",
                canonical_name(*(t.base_type)),
                fmt::join(strs.begin(), strs.end(), ",")
            );
        }

        std::string canonical_name_visitor::operator()(const EnumType& t) const
        {
            return fmt::format("Enum({0})", t.name);
        }

        std::string canonical_name_visitor::operator()(const StructureType& t) const
        {
            std::vector<std::string> strs;

            std::transform(t.fields.begin(), t.fields.end(), std::back_inserter(strs),
                [](const auto& e) { return canonical_name(*(e.second)); }
            );

            return fmt::format("Structure({0}|{1})", t.name, fmt::join(strs.begin(), strs.end(), ","));
        }

        std::string canonical_name_visitor::operator()(const ListType& t) const
        {
            return fmt::format("List({0})", canonical_name(*(t.base_type)));
        }

        std::string canonical_name_visitor::operator()(const DictionaryType& t) const
        {
            return fmt::format("Dictionary({0})", canonical_name(*(t.base_type)));
        }

        std::string canonical_name_visitor::operator()(const TupleType& t) const
        {
            std::vector<std::string> strs;

            std::transform(
                t.contained_types.begin(),
                t.contained_types.end(),
                std::back_inserter(strs),
                [](auto& t) { return canonical_name(*t); }
            );

            return fmt::format("<{0}>", fmt::join(strs.begin(), strs.end(), ","));
        }

        std::string canonical_name_visitor::operator()(const AnyType& t) const
        {
            return "any";
        }

        std::string canonical_name_visitor::operator()(const ConceptType& t) const
        {
            return fmt::format("Concept({0})", t.name);
        }

        std::string canonical_name_visitor::operator()(const GenericFunctionType& t) const
        {
            std::vector<std::string> strs;

            std::transform(
                t.generic_matches.begin(),
                t.generic_matches.end(),
                std::back_inserter(strs),
                [](const auto& t) {
                    return fmt::format("{0}:{1}",
                        t.first,
                        canonical_name(*(t.second))
                    );
                }
            );

            TypeInfo fn { t.function_type };

            return fmt::format("Generic({0},{1})",
                canonical_name(fn),
                fmt::join(strs.begin(), strs.end(), ",")
            );
        }
    }
}}