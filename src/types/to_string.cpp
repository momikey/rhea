#include "types/to_string.hpp"

namespace rhea { namespace types {
    namespace internal {
        std::string type_info_visitor::operator()(const UnknownType& t) const
        {
            return "(unknown)";
        }
        
        std::string type_info_visitor::operator()(const SimpleType& t) const
        {
            return basic_type_to_string.at(t.type);
        }
        
        std::string type_info_visitor::operator()(const NothingType& t) const
        {
            return "nothing";
        }
        
        std::string type_info_visitor::operator()(const FunctionType& t) const
        {
            std::vector<std::string> strs;

            std::transform(
                t.argument_types.begin(),
                t.argument_types.end(),
                std::back_inserter(strs),
                [](auto& t) { return fmt::format("{0} : {1}", t.first, to_string(*(t.second))); }
            );

            std::string ft;
            switch (t.definition_type)
            {
                case rhea::ast::FunctionClass::Basic:
                    ft = "function";
                    break;
                case rhea::ast::FunctionClass::Predicate:
                    ft = "predicate";
                    break;
                case rhea::ast::FunctionClass::Operator:
                    ft = "operator";
                    break;
                case rhea::ast::FunctionClass::Unchecked:
                    ft = "unchecked function";
                    break;
                default:
                    ft = "unknown";
                    break;
            }

            return fmt::format("{0} <{1}> -> {2}",
                ft,
                fmt::join(strs.begin(), strs.end(), ","),
                to_string(*(t.return_type))
            );
        }
        
        std::string type_info_visitor::operator()(const ReferenceType& t) const
        {
            return fmt::format("reference ({0})", to_string(*(t.referred_type)));
        }
        
        std::string type_info_visitor::operator()(const PointerType& t) const
        {
            return fmt::format("pointer ({0})", to_string(*(t.pointed_type)));
        }
        
        std::string type_info_visitor::operator()(const OptionalType& t) const
        {
            return fmt::format("optional ({0})", to_string(*(t.contained_type)));
        }
        
        std::string type_info_visitor::operator()(const VariantType& t) const
        {
            std::vector<std::string> strs;

            std::transform(t.types.begin(), t.types.end(), std::back_inserter(strs),
                [](auto& e) { return to_string(*e); }
            );

            return fmt::format("variant ({0})", fmt::join(strs.begin(), strs.end(), ","));
        }

        std::string type_info_visitor::operator()(const ArrayType& t) const
        {
            return fmt::format("array ({0}) [{1}]", to_string(*(t.base_type)), t.size);
        }

        std::string type_info_visitor::operator()(const GenericType& t) const
        {
            std::vector<std::string> strs;

            std::transform(t.concrete_types.begin(), t.concrete_types.end(), std::back_inserter(strs),
                [](auto& e) { return to_string(*e); }
            );

            return fmt::format("{0} <{1}>",
                to_string(*(t.base_type)),
                fmt::join(strs.begin(), strs.end(), ",")
            );
        }

        std::string type_info_visitor::operator()(const EnumType& t) const
        {
            return fmt::format("enum [{0}]", t.name);
        }
        
        std::string type_info_visitor::operator()(const StructureType& t) const
        {
            std::vector<std::string> strs;

            std::transform(
                t.fields.begin(),
                t.fields.end(),
                std::back_inserter(strs),
                [](auto& t) { return fmt::format("{0} : {1}", t.first, to_string(*(t.second))); }
            );

            return fmt::format("structure [{0}] {{{1}}}", t.name, fmt::join(strs.begin(), strs.end(), ","));
        }

        std::string type_info_visitor::operator()(const ListType& t) const
        {
            return fmt::format("list <{0}>", to_string(*(t.base_type)));
        }

        std::string type_info_visitor::operator()(const DictionaryType& t) const
        {
            return fmt::format("dictionary <{0}>", to_string(*(t.base_type)));
        }

        std::string type_info_visitor::operator()(const TupleType& t) const
        {
            std::vector<std::string> strs;

            std::transform(
                t.contained_types.begin(),
                t.contained_types.end(),
                std::back_inserter(strs),
                [](auto& t) { return to_string(*t); }
            );

            return fmt::format("tuple <{0}>", fmt::join(strs.begin(), strs.end(), ","));
        }
        
        std::string type_info_visitor::operator()(const AnyType& t) const
        {
            return "any";
        }

        std::string type_info_visitor::operator()(const ConceptType& t) const
        {
            return "concept " + t.name;
        }

        std::string type_info_visitor::operator()(const GenericFunctionType& t) const
        {
            return "generic " + to_string(t.function_type);
        }
    }
}}