#ifndef RHEA_DEBUG_INFERENCE_HPP
#define RHEA_DEBUG_INFERENCE_HPP

#include <memory>
#include <iostream>

#include "inference/engine.hpp"
#include "types/to_string.hpp"

namespace rhea { namespace debug {
    std::unique_ptr<inference::TypeEngine> build_inference_map(ast::ASTNode* ast_root)
    {
        auto engine = std::make_unique<inference::TypeEngine>();

        engine->module_scopes["main"] = std::make_unique<state::ModuleScopeTree>("main");
        engine->visitor.module_scope = engine->module_scopes["main"].get();

        ast_root->visit(&engine->visitor);

        return std::move(engine);
    }

    std::ostream& dump_module_scope(std::ostream& os, state::ModuleScopeNode* scope)
    {
        os << "Symbol table for scope " << scope->name << '\n';
        for (auto& s : scope->symbol_table)
        {
            os << s.first << '\t' << s.second->to_string() << '\n';
        }
        os << '\n';

        for (auto& c : scope->children)
        {
            dump_module_scope(os, c.get());
        }

        return os;
    }

    std::ostream& dump_inference_map(std::ostream& os, inference::TypeEngine* engine)
    {
        // Iterate over the inference map to evaluate everything.
        for (auto& e : engine->inferred_types)
        {
            e.second();
        }

        os << "\nDefined types: \n";
        for (auto& t : engine->mapper.type_map)
        {
            os << t.first << '\t' << types::to_string(t.second) << "\n";
        }

        for (auto& m : engine->module_scopes)
        {
            os << "\nModule " << m.first << ": \n";
            dump_module_scope(os, m.second->root.get());
        }

        os << "\nExpressions: \n";
        for (auto& e : engine->inferred_types)
        {
            os << e.first->to_string() << "\n\t" << types::to_string(e.second()) << '\n';
        }

        return os;
    }
}}

#endif /* RHEA_DEBUG_INFERENCE_HPP */