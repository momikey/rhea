#ifndef RHEA_AST_FUNCTION_CLASS_HPP
#define RHEA_AST_FUNCTION_CLASS_HPP

namespace rhea { namespace ast {
    // Functions can be of different classes. This enum allows us
    // to choose which class.
    enum class FunctionClass
    {
        Basic,      // i.e., not special
        Predicate,  // implied boolean return type
        Operator,   // operators are always called implicitly
        Unchecked   // can't take conditions
    };
}}

#endif /* RHEA_AST_FUNCTION_CLASS_HPP */