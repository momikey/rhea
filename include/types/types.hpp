#ifndef RHEA_TYPES_INFO_HPP
#define RHEA_TYPES_INFO_HPP

#include <algorithm>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <boost/range/combine.hpp>

#include "../ast/function_class.hpp"
#include "../util/compat.hpp"

/*
 * Type objects for Rhea data types. These use static polymorphism (variants),
 * rather than virtual functions.
 */
namespace rhea { namespace types {
    // This enum holds all the basic literal types Rhea understands.
    enum class BasicType
    {
        Integer,
        Byte,
        Float,
        Double,
        Long,
        UnsignedInteger,
        UnsignedByte,
        UnsignedLong,

        Boolean,
        String,
        Symbol,
        Any,
        Nothing,
        
        Other,          // Used for structures, user-defined types, generics, etc.
        
        Promoted,       // Used for the coercion opertor `^`
        Unknown = -1    // Error case
    };

    // Forward definition for our main type info container class
    struct TypeInfo;
    bool compatible(TypeInfo& lhs, TypeInfo& rhs);
    bool operator==(TypeInfo lhs, TypeInfo rhs);

    // An unknown type, which can go anywhere, but can't be compared. It's
    // basically a NULL type.
    struct UnknownType
    {
        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(UnknownType other) { return true; }

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // Simple types are those of literals, such as integers, doubles, strings, etc.
    struct SimpleType
    {
        SimpleType(BasicType t) : type(t) {}
        SimpleType(BasicType t, bool n): type(t), is_numeric(n), is_integral(false) {}
        SimpleType(BasicType t, bool n, bool i) : type(t), is_numeric(n), is_integral(i) {}
        BasicType type;
        bool is_numeric;
        bool is_integral;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(SimpleType other) { return type == other.type; }

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // The nothing type is special, because it can't be converted to/from, but it can
    // be used as a return type.
    struct NothingType
    {
        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(NothingType other) { return true; }

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // Function types need a map of strings to argument types, plus a return type.
    // In actuality, however, we use a vector of string/type pairs instead of a map,
    // because we also have to preserve the *order* of arguments.
    struct FunctionType
    {
        // We have to use pointers here because of the circular dependency. Using shared
        // instead of unique pointers lets us avoid a lot of messy move stuff that would
        // require some serious rewriting throughout the compiler. (I know it's a little
        // slower, and it's poor form, but we all have to make sacrifices.)

        using argument_type_pair = std::pair<std::string, std::shared_ptr<TypeInfo>>;
        std::vector<argument_type_pair> argument_types;
        std::shared_ptr<TypeInfo> return_type;
        rhea::ast::FunctionClass definition_type;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(FunctionType other);

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // An optional type just needs the contained type.
    struct OptionalType
    {
        std::shared_ptr<TypeInfo> contained_type;

        template <typename T>
        bool is_compatible(T& other);

        bool operator==(OptionalType other);

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // A variant needs multiple types.
    struct VariantType
    {
        std::vector<std::shared_ptr<TypeInfo>> types;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(VariantType other) { return types == other.types; }

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // An enum type simply needs to hold a vector of symbol names. As they aren't
    // full identifiers, we can just use strings. We also store the enum type's
    // name, to distinguish it from others that may have the same symbols.
    struct EnumType
    {
        std::string name;
        std::vector<std::string> values;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(EnumType other) { return name == other.name && values == other.values; }

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // A structure keeps a map of strings to field types.
    struct StructureType
    {
        using field_type_pair = std::pair<std::string, std::shared_ptr<TypeInfo>>;
        std::string name;
        std::vector<field_type_pair> fields;

        template <typename T>
        bool is_compatible(T& other) { return false; }
        
        bool operator==(StructureType other) { return name == other.name && fields == other.fields; }

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // The "any" type can hold anything, but what it actually holds is an implemenation detail.
    struct AnyType
    {
        // Any is technically compatible with any other type, but only if it's the LHS.
        template <typename T>
        bool is_compatible(T& other) { return true; }

        bool operator==(AnyType other) { return true; }

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // A reference type is just a reference to a contained type.
    struct ReferenceType
    {
        std::shared_ptr<TypeInfo> referred_type;

        template <typename T>
        bool is_compatible(T& other);

        bool operator==(ReferenceType other);

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // A pointer type just needs to hold a pointer.
    struct PointerType
    {
        std::shared_ptr<TypeInfo> pointed_type;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(PointerType other);

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // An array type needs both a "base" type and a size.
    struct ArrayType
    {
        std::shared_ptr<TypeInfo> base_type;
        size_t size;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(ArrayType other);

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // GenericType is intended for "specialized" generic types such as
    // `list <string>`, where all templated types are specified.
    struct GenericType
    {
        std::shared_ptr<TypeInfo> base_type;
        std::vector<std::shared_ptr<TypeInfo>> concrete_types;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(GenericType other);

        template <typename T>
        bool operator==(T other) { return false;}
    };

    // Lists are unbounded, dynamically-sized arrays.
    struct ListType
    {
        std::shared_ptr<TypeInfo> base_type;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(ListType other);

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // Dictionaries are maps betweent a hashable integer type and a given type.
    struct DictionaryType
    {
        std::shared_ptr<TypeInfo> base_type;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(DictionaryType other);

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // Tuples are unsorted, fixed-size collections of arbitarry types. For type
    // purposes, they're a little like anonymous structures, except their fields
    // don't have names.
    struct TupleType
    {
        std::vector<std::shared_ptr<TypeInfo>> contained_types;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(TupleType other);

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // Concepts are more of a compiler aid than a type that users can manipulate.
    // We'll still define them here for ease of comparison, etc.
    struct ConceptType
    {
        std::string name;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(ConceptType other) { return name == other.name; }

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // Generic functions mostly only come into play after the type inference pass,
    // but we do need to track when a function has been defined as generic.
    struct GenericFunctionType
    {
        // The function itself is fairly easy: just keep a pointer to
        // a function type info.
        FunctionType function_type;

        using match_type_pair = std::pair<std::string, std::shared_ptr<TypeInfo>>;
        std::vector<match_type_pair> generic_matches;

        template <typename T>
        bool is_compatible(T& other) { return false; }

        bool operator==(GenericFunctionType other);

        template <typename T>
        bool operator==(T other) { return false; }
    };

    // The "base" for all types. This is a variant covering all defined structs.
    using TypeInfoVariant = util::variant<
        UnknownType,
        SimpleType,
        NothingType,
        FunctionType,
        ReferenceType,
        PointerType,
        OptionalType,
        VariantType,
        EnumType,
        ArrayType,
        GenericType,
        StructureType,
        ListType,
        DictionaryType,
        TupleType,
        AnyType,
        ConceptType,
        GenericFunctionType
    >;

    // The type info container just holds an instance of the variant, and provides
    // a few helper methods to access it.
    struct TypeInfo
    {
        TypeInfo() : type_info_v(UnknownType()) {}

        template<typename T>
        TypeInfo(T t) : type_info_v(t) {}

        TypeInfoVariant& type() { return type_info_v; }

        private:
        TypeInfoVariant type_info_v;
    };

    ////
    // Specializations for type classes
    ////

    // Scalar types are only compatible with themselves. (We can do things like promotion
    // elsewhere in the compiler.)
    template <>
    inline bool SimpleType::is_compatible(SimpleType& other)
    {
        return type == other.type;
    }

    // The nothing type is only compatible with itself.
    template <>
    inline bool NothingType::is_compatible(NothingType& other)
    {
        return true;
    }

    // Function types are compatible if their signatures are.
    template <>
    inline bool FunctionType::is_compatible(FunctionType& other)
    {
         return (argument_types == other.argument_types &&
            return_type == other.return_type &&
            definition_type == other.definition_type);
    }
    
    // Optionals types are compatible with other optionals holding the same type
    // *or* that type.
    template <typename T>
    inline bool OptionalType::is_compatible(T& other)
    {
        return util::visit(
            [&](auto& ct) { return other.is_compatible(ct); },
            contained_type->type()
        );
    }

    template <>
    inline bool OptionalType::is_compatible(OptionalType& other)
    {
        return *contained_type == *(other.contained_type);
    }

    template <>
    inline bool EnumType::is_compatible(EnumType& other)
    {
        return name == other.name && values == other.values;
    }

    // Structures are compatible with structures that have the exact same layout.
    template <>
    inline bool StructureType::is_compatible(StructureType& other)
    {
         return fields == other.fields;
    }

    // References are compatible with other references of the same type *and*
    // instances of their referred type. (Rhea allows auto-dereferencing.)
    template <typename T>
    inline bool ReferenceType::is_compatible(T& other)
    {
        return util::visit(
            [&](auto& rt) { return other.is_compatible(rt); },
            referred_type->type()
        );
    }

    template <>
    inline bool ReferenceType::is_compatible(ReferenceType& other)
    {
        return *referred_type == *(other.referred_type);
    }

    // Pointers do *not* auto-dereference, and they are incompatible with
    // other pointer types.
    template <>
    inline bool PointerType::is_compatible(PointerType& other)
    {
        return *pointed_type == *(other.pointed_type);
    }

    // Arrays are only compatible if their base types are compatible *and*
    // they have the same size.
    template <>
    inline bool ArrayType::is_compatible(ArrayType& other)
    {
        return compatible(*base_type, *(other.base_type)) && size == other.size;
    }

    // Lists are compatible if their types match. Size doesn't matter, as they
    // resize as needed.
    template <>
    inline bool ListType::is_compatible(ListType& other)
    {
        return compatible(*base_type, *(other.base_type));
    }

    // Dictionaries only care about the type of their values; the keys can be
    // any integral type that hashes. (ATM, integers and symbols)
    template <>
    inline bool DictionaryType::is_compatible(ListType& other)
    {
        return compatible(*base_type, *(other.base_type));
    }

    // Generics are only compatible if their types are. (We aren't ready to
    // worry about covariance yet.)
    template <>
    inline bool GenericType::is_compatible(GenericType& other)
    {
        if (!compatible(*base_type, *(other.base_type)) ||
            concrete_types.size() != other.concrete_types.size())
        {
            return false;
        }
        else
        {
            auto combined = boost::combine(concrete_types, other.concrete_types);
            return std::all_of(combined.begin(), combined.end(),
                [] (const auto& e)
                {
                    std::shared_ptr<TypeInfo> lhs, rhs;
                    boost::tie(lhs, rhs) = e;
                    return compatible(*lhs, *rhs);
                }
            );
        }
    }

    // Tuples are compatible if and only if they have identical type lists.
    template <>
    inline bool TupleType::is_compatible(TupleType& other)
    {
        auto combined = boost::combine(contained_types, other.contained_types);
        return std::all_of(combined.begin(), combined.end(),
            [] (const auto& e)
            {
                std::shared_ptr<TypeInfo> lhs, rhs;
                boost::tie(lhs, rhs) = e;
                return compatible(*lhs, *rhs);
            }
        );
    }

    template <>
    inline bool ConceptType::is_compatible(ConceptType& other)
    {
        return name == other.name;
    }

    inline bool FunctionType::operator==(FunctionType other)
    {
        return argument_types == other.argument_types &&
            *return_type == *(other.return_type) &&
            definition_type == other.definition_type;
    }

    inline bool ReferenceType::operator==(ReferenceType other)
    {
        return *referred_type == *(other.referred_type);
    }

    inline bool PointerType::operator==(PointerType other)
    {
        return *pointed_type == *(other.pointed_type);
    }

    inline bool OptionalType::operator==(OptionalType other)
    {
        return *contained_type == *(other.contained_type);
    }

    inline bool ArrayType::operator==(ArrayType other)
    {
        return *base_type == *(other.base_type) && size == other.size;
    }

    inline bool ListType::operator==(ListType other)
    {
        return *base_type == *(other.base_type);
    }

    inline bool DictionaryType::operator==(DictionaryType other)
    {
        return *base_type == *(other.base_type);
    }

    inline bool GenericType::operator==(GenericType other)
    {
        if (*base_type == *(other.base_type) &&
            concrete_types.size() == other.concrete_types.size())
        {
            auto combined = boost::combine(concrete_types, other.concrete_types);
            return std::all_of(combined.begin(), combined.end(),
                [] (const auto& e)
                {
                    std::shared_ptr<TypeInfo> lhs, rhs;
                    boost::tie(lhs, rhs) = e;
                    return *lhs == *rhs;
                }
            );
        }
        else
        {
            return false;
        }
    }

    inline bool TupleType::operator==(TupleType other)
    {
        return is_compatible(other);
    }

    inline bool GenericFunctionType::operator==(GenericFunctionType other)
    {
        return (function_type == other.function_type) &&
            generic_matches == other.generic_matches;
    }

    // Comparison function. This *only* checks for exact matches at this time.
    inline bool compatible(TypeInfo& lhs, TypeInfo& rhs)
    {
        auto fn = [&](auto& l, auto& r) { return l.is_compatible(r); };
        return util::visit(fn, lhs.type(), rhs.type());
    }

    inline bool compatible(TypeInfo lhs, TypeInfo rhs)
    {
        auto fn = [&](auto& l, auto& r) { return l.is_compatible(r); };
        return util::visit(fn, lhs.type(), rhs.type());
    }

    // The comparison operator delegates to the variants.
    inline bool operator==(TypeInfo lhs, TypeInfo rhs)
    {
        return lhs.type().index() == rhs.type().index() &&
            util::visit([](auto& l, auto& r) {
                return l == r;
            }, lhs.type(), rhs.type());
    }

}}

#endif /* RHEA_TYPES_INFO_HPP */