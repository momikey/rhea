#ifndef RHEA_TYPES_TO_STRING_HPP
#define RHEA_TYPES_TO_STRING_HPP

#include <algorithm>
#include <iterator>
#include <string>
#include <vector>

#include <fmt/format.h>

#include "types.hpp"
#include "basic_type_map.hpp"
#include "../util/compat.hpp"

/*
 * Convert Rhea "type info" structures to strings, for use in error messages, etc.
 */
namespace rhea { namespace types {
    namespace internal {
        // Use a visitor functor, because some of the possible types are nested,
        // and we can't do that with template specializations.
        struct type_info_visitor
        {
            std::string operator()(const UnknownType& t) const;
            std::string operator()(const SimpleType& t) const;
            std::string operator()(const NothingType& t) const;
            std::string operator()(const FunctionType& t) const;
            std::string operator()(const ReferenceType& t) const;
            std::string operator()(const PointerType& t) const;
            std::string operator()(const OptionalType& t) const;
            std::string operator()(const VariantType& t) const;
            std::string operator()(const ArrayType& t) const;
            std::string operator()(const GenericType& t) const;
            std::string operator()(const EnumType& t) const;
            std::string operator()(const StructureType& t) const;
            std::string operator()(const ListType& t) const;
            std::string operator()(const DictionaryType& t) const;
            std::string operator()(const TupleType& t) const;
            std::string operator()(const AnyType& t) const;
            std::string operator()(const ConceptType& t) const;
            std::string operator()(const GenericFunctionType& t) const;

            template <typename T>
            std::string operator()(T t) const { return "unknown"; }
        };

        // This template is a helper for other modules that may want to access
        // the type visitor.
        template <typename T>
        std::string to_string(const T& tinfo) { return type_info_visitor()(tinfo); }

    }

    inline std::string to_string(TypeInfo tinfo) {
        return util::visit(internal::type_info_visitor(), tinfo.type());
    }
}}

#endif /* RHEA_TYPES_TO_STRING_HPP */