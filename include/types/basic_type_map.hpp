#ifndef RHEA_TYPES_BASIC_MAP_HPP
#define RHEA_TYPES_BASIC_MAP_HPP

#include <string>
#include <unordered_map>

#include "./types.hpp"

namespace rhea { namespace types {
    /*
     * A mapping from the "basic type" enum to strings.
     */
    static const std::unordered_map<BasicType, std::string> basic_type_to_string = {
        { BasicType::Integer,           "integer" },
        { BasicType::Byte,              "byte" },
        { BasicType::Long,              "long" },
        { BasicType::UnsignedInteger,   "uinteger" },
        { BasicType::UnsignedByte,      "ubyte" },
        { BasicType::UnsignedLong,      "ulong" },
        { BasicType::Float,             "float" },
        { BasicType::Double,            "double" },
        { BasicType::Boolean,           "boolean" },
        { BasicType::Symbol,            "symbol" },
        { BasicType::String,            "string" },
        { BasicType::Any,               "any" },
        { BasicType::Nothing,           "nothing" },
        { BasicType::Promoted,          "promoted" },
        { BasicType::Other,             "other" },

        { BasicType::Unknown,           "unknown" }
    };

    /*
     * A mapping from strings to their corresponding basic types.
     */
    static const std::unordered_map<std::string, BasicType> string_to_basic_type = {
        { "integer",    BasicType::Integer },
        { "byte",       BasicType::Byte },
        { "long",       BasicType::Long },
        { "uinteger",   BasicType::UnsignedInteger },
        { "ubyte",      BasicType::UnsignedByte },
        { "ulong",      BasicType::UnsignedLong },
        { "float",      BasicType::Float },
        { "double",     BasicType::Double },
        { "boolean",    BasicType::Boolean },
        { "symbol",     BasicType::Symbol },
        { "string",     BasicType::String },
        { "any",        BasicType::Any },
        { "nothing",    BasicType::Nothing },

        { "unknown",    BasicType::Unknown }
    };
}}

#endif /* RHEA_TYPES_BASIC_MAP_HPP */