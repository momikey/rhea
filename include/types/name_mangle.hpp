#ifndef RHEA_TYPES_NAME_MANGLE_HPP
#define RHEA_TYPES_NAME_MANGLE_HPP

#include <string>
#include <type_traits>

#include <fmt/format.h>

#include "../ast/error.hpp"
#include "../ast/nodes/function.hpp"
#include "../util/compat.hpp"

#include "types.hpp"
#include "to_string.hpp"

namespace rhea { namespace types {
    using namespace std::string_literals;

    using rhea::ast::FunctionClass;

    /*
     * As Rhea supports functions overloaded on the basis of their argument and
     * return types, it must have some form of name-mangling to allow the linker
     * to keep track of which overload is being called.
     */

    // Given the unmangled name of a function and a type info object describing it,
    // produce a mangled name.
    std::string mangle_function_name(std::string name, FunctionType function_type,
        FunctionClass function_class = FunctionClass::Basic);

    namespace internal {
        // template <typename T>
        // std::string mangle_argument_name(T& argument_type)
        // { throw rhea::ast::unimplemented_type(to_string(argument_type)); }
        struct name_mangle_visitor
        {
            std::string operator()(const UnknownType& t) const;
            std::string operator()(const SimpleType& t) const;
            std::string operator()(const NothingType& t) const;
            // Rhea doesn't have first-class functions.
            // std::string operator()(const FunctionType& t) const;
            std::string operator()(const ReferenceType& t) const;
            std::string operator()(const PointerType& t) const;
            std::string operator()(const OptionalType& t) const;
            std::string operator()(const VariantType& t) const;
            std::string operator()(const ArrayType& t) const;
            std::string operator()(const GenericType& t) const;
            std::string operator()(const EnumType& t) const;
            std::string operator()(const StructureType& t) const;
            std::string operator()(const ListType& t) const;
            std::string operator()(const DictionaryType& t) const;
            std::string operator()(const TupleType& t) const;
            std::string operator()(const AnyType& t) const;
            
            // Concepts are for generic programming. Since they're not concrete
            // types, they don't need to be mangled.

            // For generic functions, we'll work things out on the enclosed function
            // type info object.

            template <typename T>
            std::string operator()(T t) const { throw rhea::ast::unimplemented_type(to_string(t)); }
        };
    }

    inline std::string mangle_name(TypeInfo tinfo)
    {
        return util::visit(internal::name_mangle_visitor(), tinfo.type());
    }
    // TODO: Go the other way, from a mangled function name into a type info object.
    // This might be needed for overload resolution or something.
}}

#endif /* RHEA_TYPES_NAME_MANGLE_HPP */