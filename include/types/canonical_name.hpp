#ifndef RHEA_TYPES_CANONICAL_NAME_HPP
#define RHEA_TYPES_CANONICAL_NAME_HPP

#include <algorithm>
#include <string>
#include <vector>

#include <fmt/format.h>

#include "types.hpp"
#include "basic_type_map.hpp"
#include "../util/compat.hpp"

/*
 * Create a "canonical" string representation of a Rhea type. This isn't the same
 * as the `to_string` printer. That is for human-readable type names, while this
 * module is intended only for internal storage.
 */
namespace rhea { namespace types {
    namespace internal {
        // We use the same variant visitor pattern as in the string printer.
        struct canonical_name_visitor
        {
            std::string operator()(const UnknownType& t) const;
            std::string operator()(const SimpleType& t) const;
            std::string operator()(const NothingType& t) const;
            std::string operator()(const FunctionType& t) const;
            std::string operator()(const ReferenceType& t) const;
            std::string operator()(const PointerType& t) const;
            std::string operator()(const OptionalType& t) const;
            std::string operator()(const VariantType& t) const;
            std::string operator()(const ArrayType& t) const;
            std::string operator()(const GenericType& t) const;
            std::string operator()(const EnumType& t) const;
            std::string operator()(const StructureType& t) const;
            std::string operator()(const ListType& t) const;
            std::string operator()(const DictionaryType& t) const;
            std::string operator()(const TupleType& t) const;
            std::string operator()(const AnyType& t) const;
            std::string operator()(const ConceptType& t) const;
            std::string operator()(const GenericFunctionType& t) const;

            template <typename T>
            std::string operator()(T t) const { return "Unknown"; }
        };
    }

    inline std::string canonical_name(TypeInfo& tinfo)
    {
        return util::visit(internal::canonical_name_visitor(), tinfo.type());
    }
}}

#endif /* RHEA_TYPES_CANONICAL_NAME_HPP */