#ifndef RHEA_INFERENCE_RETURN_LIST_HPP
#define RHEA_INFERENCE_RETURN_LIST_HPP

#include <vector>

#include "ast.hpp"

namespace rhea { namespace inference {
    /*
     * The return list is just a mapping of Return AST nodes to the
     * function definition in which they are contained. It's intended
     * to be an aid to type inference.
     */
    struct ReturnList
    {
        ast::Def* function;
        std::vector<ast::Return*> returns;
    };
}}

#endif /* RHEA_INFERENCE_RETURN_LIST_HPP */
