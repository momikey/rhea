#ifndef RHEA_INFERENCE_LAZY_TYPE_HPP
#define RHEA_INFERENCE_LAZY_TYPE_HPP

#include <vector>
#include <functional>

#include "../types/types.hpp"
#include "../ast.hpp"
#include "engine_fwd.hpp"
#include "return_list.hpp"

/*
 * We define a "lazy" type inference using a lambda. This way,
 * the type doesn't have to be calculated until it's actually
 * used, and it can be updated as more information is discovered.
 * 
 * One small problem is that lambdas with captures can't be stored
 * as map values, because they don't have an implicit conversion
 * to function pointer. So we'll make an object that holds a lambda
 * without captures, as well as pointers to the type engine, the
 * node we're evaluating, and any state necessary to reconstruct
 * the node's environment.
 */
namespace rhea { namespace inference {
    using namespace rhea::ast;

    /*
     * A state object to be passed to the inference functor. This is
     * intended to recreate the engine's state at the moment of
     * definition, because we're not allowed to capture it while
     * storing the functors.
     * 
     * We'll pass this by value, so it's good to keep it light.
     * Mostly, it should just contain (non-owning) pointers.
     */
    struct InferenceState
    {
        // A pointer to the node's scope.
        state::ModuleScopeNode* scope;
        ReturnList return_list;
    };

    // A type inference functor holding a lambda which will evaluate
    // to the proper TypeInfo object.
    struct InferredType
    {
        // A function object representing how to create the type.
        using function_type = std::function<types::TypeInfo(TypeEngine*, ASTNode*, InferenceState)>;

        function_type function = [](TypeEngine*, ASTNode*, InferenceState){ return types::UnknownType(); };

        TypeEngine* engine;
        ASTNode* node;
        InferenceState state;

        types::TypeInfo operator()() { return function(engine, node, state); }
    };
}}

#endif /* RHEA_INFERENCE_LAZY_TYPE_HPP */