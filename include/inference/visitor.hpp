#ifndef RHEA_INFERENCE_VISITOR_HPP
#define RHEA_INFERENCE_VISITOR_HPP

#include <algorithm>
#include <memory>
#include <stack>
#include <utility>
#include <vector>

#include <boost/range/adaptor/reversed.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include "ast.hpp"
#include "state/module_tree.hpp"
#include "types/canonical_name.hpp"
#include "types/to_string.hpp"
#include "util/compat.hpp"
#include "visitor/default.hpp"
#include "visitor/visitor.hpp"

#include "engine_fwd.hpp"
#include "lazy_type.hpp"
#include "return_list.hpp"

/*
 * The type inference engine needs a visitor, too. This uses the same
 * double dispatch pattern as codegen, but it's geared toward building
 * the map of inferred types for each part of the AST.
 */
namespace rhea { namespace inference {
    using namespace rhea::ast;
    using util::any;

    struct InferenceVisitor : visitor::DefaultVisitor
    {
        InferenceVisitor(TypeEngine* e) : engine(e) {}

        TypeEngine* engine = nullptr;

        // Visitor state; the engine itself doesn't care about this

        // A pointer to the scope tree for the current module
        state::ModuleScopeTree* module_scope;

        // Function definition return nodes.
        std::stack<ReturnList> function_definition_returns;

        // We'll eventually override pretty much all the node-specific methods.
        any visit(Boolean* n) override;
        any visit(Integer* n) override;
        any visit(Byte* n) override;
        any visit(Long* n) override;
        any visit(UnsignedInteger* n) override;
        any visit(UnsignedByte* n) override;
        any visit(UnsignedLong* n) override;
        any visit(Float* n) override;
        any visit(Double* n) override;
        any visit(String* n) override;
        any visit(Symbol* n) override;
        any visit(Nothing* n) override;

        any visit(BinaryOp* n) override;
        any visit(UnaryOp* n) override;
        any visit(TernaryOp* n) override;
        any visit(Subscript* n) override;
        any visit(Member* n) override;
        any visit(Call* n) override;
        any visit(NamedArgument* n) override;

        any visit(Identifier* n) override;
        any visit(FullyQualified* n) override;
        any visit(RelativeIdentifier* n) override;

        any visit(BuiltinType* n) override;
        any visit(SimpleTypename* n) override;
        any visit(GenericTypename* n) override;
        any visit(VectorTypename* n) override;
        any visit(Variant* n) override;
        any visit(Optional* n) override;
        any visit(Cast* n) override;
        any visit(TypeCheck* n) override;
        any visit(Alias* n) override;
        any visit(Enum* n) override;
        any visit(SymbolList* n) override;
        any visit(Array* n) override;
        any visit(List* n) override;
        any visit(Tuple* n) override;
        any visit(DictionaryEntry* n) override;
        any visit(Dictionary* n) override;
        
        any visit(If* n) override;
        any visit(BareExpression* n) override;
        any visit(Return* n) override;
        any visit(Assign* n) override;
        any visit(CompoundAssign* n) override;
        any visit(Block* n) override;
        any visit(While* n) override;
        any visit(For* n) override;
        any visit(With* n) override;
        any visit(Break* n) override;
        any visit(Continue* n) override;
        any visit(Match* n) override;
        any visit(On* n) override;
        any visit(When* n) override;
        any visit(TypeCase* n) override;
        any visit(Default* n) override;
        any visit(PredicateCall* n) override;
        any visit(TypeDeclaration* n) override;
        any visit(Variable* n) override;
        any visit(Constant* n) override;
        any visit(Structure* n) override;

        any visit(Def* n) override;
        any visit(GenericDef* n) override;
        any visit(Arguments* n) override;
        any visit(TypePair* n) override;
        any visit(Try* n) override;
        any visit(Catch* n) override;
        any visit(Throw* n) override;
        any visit(Finally* n) override;
        any visit(Extern* n) override;

        any visit(Concept* n) override;
        any visit(ConceptMatch* n) override;
        any visit(FunctionCheck* n) override;

        any visit(ModuleDef* n) override;
        any visit(Import* n) override;
        any visit(Export* n) override;
        any visit(Use* n) override;

        any visit(Program* n) override;
        any visit(Module* n) override;
    };
}}

#endif /* RHEA_INFERENCE_VISITOR_HPP */