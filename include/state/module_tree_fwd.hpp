#ifndef RHEA_STATE_MODULE_TREE_FWD_HPP
#define RHEA_STATE_MODULE_TREE_FWD_HPP

/*
 * Forward definition for module scope tree.
 */
namespace rhea { namespace state {
    struct ModuleScopeTree;
}}

#endif /* RHEA_STATE_MODULE_TREE_FWD_HPP */