#ifndef RHEA_STATE_MODULE_NODE_HPP
#define RHEA_STATE_MODULE_NODE_HPP

#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "../ast/nodes/node_base.hpp"

#include "module_tree_fwd.hpp"

/*
 * Nodes for the module symbol table/scope tree.
 */
namespace rhea { namespace state {
    struct ModuleScopeNode
    {
        ModuleScopeNode(std::string n, ModuleScopeNode* p);
        ModuleScopeNode(std::string n, ModuleScopeTree* m, ModuleScopeNode* p);
        
        ModuleScopeNode();
        ModuleScopeNode(ModuleScopeNode* p);
        ModuleScopeNode(std::string n);

        // Find an AST node in this scope or its ancestors. Some parts of the compiler
        // may need to do this with only a pointer to a scope, so we add a method here.
        ast::ASTNode* find_symbol(std::string sym);

        // Find all the functions with the given name that have been defined in this
        // scope. This returns a pair of iterators.
        auto find_function_definitions(std::string sym)
        { return function_definitions.equal_range(sym); }
        
        // The name of this scope. Most scopes will be unnamed, but something such as
        // a function definition will have its name here.
        std::string name;

        // The symbol table for this scope, linking identifier names defined in the
        // scope with the AST nodes that define them.
        std::unordered_map<std::string, ast::ASTNode*> symbol_table;

        // For function definitions, we use a multimap, because overloading allows
        // more than one function with the same name.
        std::multimap<std::string, ast::ASTNode*> function_definitions;

        // Non-owning pointer to link directly to the containing module for this scope.
        ModuleScopeTree* containing_module;

        // Non-owning pointer to this scope node's parent.
        ModuleScopeNode* parent;

        // Owning pointers to this scope node's children.
        std::vector<std::unique_ptr<ModuleScopeNode>> children;
    };
}}

#endif /* RHEA_STATE_MODULE_NODE_HPP */