#ifndef RHEA_GRAMMAR_TYPENAMES_HPP
#define RHEA_GRAMMAR_TYPENAMES_HPP

#include <tao/pegtl.hpp>

#include "tokens.hpp"
#include "operator.hpp"
#include "expression_fwd.hpp"

namespace rhea { namespace grammar {
    using namespace tao::pegtl;

    // Forward declarations
    struct type_name;
    struct base_type_name;
    struct vector_type_expression;

    // The "simple" builtin types for Rhea, i.e., those represented
    // by single identifiers.
    struct builtin_types : sor <
        kw_integer,
        kw_byte,
        kw_long,
        kw_uinteger,
        kw_ubyte,
        kw_ulong,
        kw_boolean,
        kw_string,
        kw_any,
        kw_nothing        
    > {};

    // Pointers and references count as modifiers to type names.
    struct ptr_reference_type : sor <kw_ref, kw_ptr> {};

    // Different parts of a more complex type name.

    // The generic part of a type.
    struct generic_specifier : if_must <
        seq <
            one <'<'>,
            pad <
                list <type_name, one <','>, ignored>,
                ignored
            >
        >,
        one <'>'>
    > {};

    // An array modifier for a type. (e.g., `integer [42]`).
    // The reason we name it `vector_type` here is because we'll be using the
    // postfix operator transformation to handle recursive definitions, and that
    // puts this node as the parent.
    struct vector_type : if_must <
        seq <
            one <'['>,
            pad <constant_expression, ignored>
        >,
        one <']'>
    > {};

    // Simple types are just an identifier (possibly in another module) or one of
    // Rhea's built-in types.
    struct simple_type_name : sor <
        builtin_types,
        any_identifier
    > {};

    // Pointers and references create new type names.
    struct pointer_or_reference_name : seq <
        ptr_reference_type,
        separator,
        type_name
    > {};

    // A generic type is a simple type plus a generic specifier.
    struct generic_type : seq <
        simple_type_name,
        separator,
        generic_specifier
    > {};

    // Optionals and variants can't be nested, so we need a special case.
    struct fixed_type : sor <
        generic_type,
        vector_type_expression,
        pointer_or_reference_name,
        simple_type_name
    > {};    

    // Variants have type "lists".
    struct variant_type_list : list <fixed_type, one <','>, ignored> {};

    // A tagged union or variant type. We use the former name here so as not to
    // step on the C++ standard library's toes.
    struct tagged_union : seq <
        one <'|'>,
        if_must <
            variant_type_list,
            one <'|'>
        >,
        not_at <separator, one <'?'> >
    > {};

    // An optional type, which is really just a variant of one type and "nothing".
    struct optional_type : seq <
        one <'|'>,
        if_must <
            at <until <one <'|'> >, separator, one <'?'> >,
            fixed_type,
            one <'|'>
        >,
        separator,
        one <'?'>
    > {};

    // A scalar type name is any type without an array component.
    struct scalar_type : sor <
        generic_type,
        tagged_union,
        optional_type,
        pointer_or_reference_name,
        simple_type_name
    > {};

    // Vector types have an array component. They can be nested, too, for multi-dimensional
    // array. This means we have to handle recursion, and it's naturally left-recursion.
    // We can use the same strategy as with postfix operators to handle this.
    struct vector_type_expression : seq <
        scalar_type,
        star <
            pad < vector_type, ignored>
        >
    > {};

    // Type specifiers

    struct return_type_spec : seq <
        return_type_operator,
        separator,
        type_name
    > {};

    // Now we can define a general type name.
    struct type_name : sor <
        vector_type_expression,
        scalar_type
    > {};

    // External types are any builtin or *static* user-defined type.
    struct external_type : sor <
        pointer_or_reference_name,
        simple_type_name
    > {};

    // A type pair is a key-value pair where the value is a type name.
    // This is used in structure and concept definitions, as well as
    // catching exceptions.
    struct type_pair : seq <
        identifier,
        separator,
        one <':'>,
        separator,
        type_name
    > {};

    // A constructor expression calls the `new$` operator on a generic
    // type of some sort.
    struct constructor_expression : seq <
        generic_type,
        separator,
        function_call_expr
    > {};

    // Type definitions come in two forms: structures and enums. Enums
    // are simpler, in that they're just a symbol list. Structures take
    // a list of type pairs. Both start the same way.
    struct type_definition_lhs : seq <
        kw_type,
        separator,
        identifier,
        separator,
        assignment_operator
    > {};

    // Type assertions are used in the match-type statement. We define
    // them as unable to use variants/optionals, since the whole purpose
    // of the construct is to let us match against those.
    struct type_assertion : seq <
        kw_type,
        separator,
        sor <
            generic_type,
            pointer_or_reference_name,
            simple_type_name
        >
    > {};

    // Type and concept matching
    struct type_match : type_pair {};
}}

#endif /* RHEA_GRAMMAR_TYPENAMES_HPP */